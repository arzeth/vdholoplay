/*
import io from 'socket.io'
//import {Server as p2p} from 'socket.io-p2p-server'
import http from 'http'
import cookie from 'cookie'
//import uuidv4 from 'uuid/v4'
import uuidv4 from 'uuid/v4'
*/

const io = require('socket.io')
//const Server = require('socket.io-p2p-server')
const http = require('http')
const cookie = require('cookie')
const uuidv4 = require('uuid/v4')

const RESP_ACK = 'ACK'

const FPS = 60

const TIMEOUT_OVERHEAD = 2

// https://developer.valvesoftware.com/wiki/Source_Multiplayer_Networking#Basic_networking
// The server simulates the game in discrete time steps called ticks. By default, the timestep is 15ms, so 66.666... ticks per second are simulated
const LOG_PUSH_LATENCY = 15 - TIMEOUT_OVERHEAD // Counter-Strike is 15, Left 4 Dead is 30
// but since we want EVERYTHING to be smooth, the spectator should begin processing the FIRST (!) item of the queue with a delay of (ping*1.5-15)ms

function isValidUuid (input)
{
	return /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i.test(input)
}

function isValidSessionId (input)
{
	return /^\d{5}$/.test(input)
}

class User {
	constructor (uuid) {
		if (!isValidUuid(uuid)) throw new Error('Invalid UUID: ' + uuid)
		Object.defineProperties(this, {
			uuid: {
				value: uuid,
				enumerable: true
			},
			/*queue: {
				value: [],
				enumerable: true
			},*/
			socketIds: {
				value: [],
				enumerable: true
			},
		})
	}
}

/*class LogItem {
	constructor (state, timestamp) {
		if (timestamp instanceof Date) throw 'bad one'
		this.state = state
		this.timestamp = timestamp
	}
}*/
class Session {
	constructor (_onDie, id, hostSockets)
	{
		if (typeof _onDie !== 'function') throw 'Session:: _onDie is not a function!'
		if (hostSockets && !Array.isArray(hostSockets)) throw 'hostSockets is not a map'
		hostSockets = new Map(hostSockets.map(x => [x, {}]))
		Object.defineProperties(this, {
			_onDie: {
				value: _onDie
			},
			isAlive: {
				value: true,
				writable: true
			},
			id: {
				value: id,
				enumerable: true
			},
			hostSockets: {
				value: hostSockets,
				enumerable: true
			},
			spectatorSockets: {
				value: new Map(), // socket => {ping, logVersion, _interval}
				enumerable: true
			},
			log: {
				value: [],
				enumerable: true
			}
		})
	}

	get latestLogVersion ()
	{
		const x = this.log.length
		if (x === 0) return null
		return x - 1
	}

	sendWebrtcSignalToSocket (socket)
	{
		if (this.webrtcSignalData)
		{
			console.log('99', socket, this.id, this.webrtcSignalData)
			setTimeout(() => socket.emit('webrtc-signal', this.id, this.webrtcSignalData), 1000)
		}
	}

	sendWebrtcSignalToAll ()
	{
		console.log('106')
		this.spectatorSockets.forEach((_, socket) => {
			this.sendWebrtcSignalToSocket(socket)
		})
	}

	addSpectatorSocket (socket, user)
	{
		this.spectatorSockets.set(
			socket,
			{user, logVersion: null, _interval: null}
		)
		this.sendWebrtcSignalToSocket(socket)
	}

	removeSpectatorSocket (rms)
	{
		this.spectatorSockets.forEach((socketInfo, socket) => {
			clearInterval(socketInfo._interval)
			socketInfo._interval = null
			if (socket === rms) this.spectatorSockets.delete(socket)
		})
	}

	addHostSocket (socket)
	{
		this.hostSockets.set(socket, {})
	}

	removeHostSocket (rms)
	{
		if (this.hostSockets.size === 1 && this.hostSockets.has(rms))
		{
			this.die()
		}
		else
		{
			this.hostSockets.forEach((_, socket) => {
				if (socket === rms) this.hostSockets.delete(socket)
			})
		}
	}

	emit (type, ...args)
	{
		this.spectatorSockets.forEach((_, socket) => {
		//for (let i = 0; i < this.sockets.length; i++) {
			//const socket = this.sockets[i]
			//console.log('Session.emit (%O) {', socket.vdPing)
			//console.log([type, this.id, ...args])
			//console.log('Session.emit }')
			//setTimeout(() => {
				socket.emit(type, this.id, ...args)
			//}, socket.vdPing > 160 ? 500 : 500 - socket.vdPing)
		})
	}

	die ()
	{
		this.isAlive = false
		this.emit('sessionDied')
		//clearInterval(this._interval)
		this.spectatorSockets.forEach((_, socket) => {
			//socket.emit('leaveSession', this.id)
			this.removeSpectatorSocket(socket) // what is the point if the class instance is going to be deleted anyway
		})
		this._onDie()
		/*this.hostSockets.forEach((_, socket) => {
			//socket.emit('leaveSession', this.id)
			this.removeHostSocket(socket) // what is the point if the class instance is going to be deleted anyway
		})*/
	}

	addLogItems (items, useMaximumSpeed)
	{
		if (items.length === 0) return
		//console.log(items)
		//items.forEach(x => this._addLogItem(x.logVersion, x.timestamp, x.state))

		for (let i = 0; i < items.length; i++)
		{
			this._addLogItem(items[i][0], items[i][1], items[i][2])
		}

		this.spectatorSockets.forEach((socketInfo, socket) => {
			this.sendAvailableUnreadLogsToSocket(socket)
			return



			if (useMaximumSpeed)
			{
				this._disableAutomaticQueueSendingToSocket(socket)
				this.sendAvailableUnreadLogsToSocket(socket)
				//this._enableAutomaticQueueSendingToSocket(socket)
			}
			else if (socketInfo._interval === null)
			{
				if (items.length === 1 && items[0].inputMethod === 'k')
				{
					//console.log('inputMethod = k!!')
					this.sendAvailableUnreadLogsToSocket(socket)
				}
				else if (items.length === 2 && items[0].inputMethod === 'k' && items[1].inputMethod === 'k')
				{
					//console.log('inputMethod = k2!!')
					this.sendAvailableUnreadLogsToSocket(socket)
				}
				else
				{
					this._enableAutomaticQueueSendingToSocket(socket)
				}
			}
		})
	}

	_addLogItem (logVersion, timestamp, state, inputMethod) {
		//console.log('addLogItem(%O, %O, %O, %O)', logVersion, timestamp, state, inputMethod)
		if (this.log[logVersion]) return // already exists
		this.log[logVersion] = {logVersion, timestamp, state, timesSent: 0}

		/*const logLength = this.log.length
		if (logLength)
		{
			const lastItem = this.log[logLength - 1]
			if (lastItem)
			{
				if (JSON.stringify(lastItem.state) === JSON.stringify(state))
				{
					// what's the point
					return
				}
				if (state.timestamp <= lastItem.timestamp)
				{
					// probably we already got we needed
					return
				}
			}
		}

		this.log.push(new LogItem(state, timestamp))*/
	}

	_enableAutomaticQueueSendingToSocket (socket) {
		/*const socketInfo = this.spectatorSockets.get(socket)

		clearInterval(socketInfo._interval) // for safety
		socketInfo._interval = setInterval(() => {
			this.sendAvailableUnreadLogsToSocket(socket)
		//}, Math.ceil(socket.vdPing / 2))
		}, LOG_PUSH_LATENCY)*/

	}

	_disableAutomaticQueueSendingToSocket (socket) {
		/*const socketInfo = this.spectatorSockets.get(socket)
		clearInterval(socketInfo._interval)
		socketInfo._interval = null*/
	}

	sendAvailableUnreadLogsToSocket (socket) {
		const latestLogVersion = this.log.length ? this.log.length - 1 : null
		if (latestLogVersion === null) return
		const socketInfo = this.spectatorSockets.get(socket)
		const {logVersion} = socketInfo
		if (logVersion > latestLogVersion) throw `impossible bug: ${logVersion} > ${latestLogVersion}`
		if (logVersion === latestLogVersion) return
		this.sendLog(
			socket,
			logVersion ? logVersion + 1 : 0,
			latestLogVersion,
			() => {
				//console.log('can we stop interval: %O cmp %O', socketInfo.logVersion, this.log.length - 1)
				//if (logVersion === this.log.length - 1) // don't use this line!!1
				if (socketInfo.logVersion === this.log.length - 1)
				{
					this._disableAutomaticQueueSendingToSocket(socket)
				}
			}
		)
	}
	/*sendAvailableUnreadLogsToSockets () {
		const latestLogVersion = this.log.length ? this.log.length - 1 : null
		if (latestLogVersion === null) return
		this.spectatorSockets.forEach(({logVersion}, socket) => {
			if (logVersion > latestLogVersion) throw `impossible bug: ${logVersion} > ${latestLogVersion}`
			if (logVersion === latestLogVersion) return
			this.sendLog(socket, logVersion ? logVersion + 1 : 0, latestLogVersion)
		})
	}*/

	sendLog (socket, fromVersion, tillVersion, callbackOnAck) {
		const sockInfo = this.spectatorSockets.get(socket)

		const MAX_TIMES_SEND_REPEAT = 2

		let repeater = null

		const logItemsSlice = (
			this.log.slice(fromVersion, tillVersion + 1)
		)
		const sendMessage = (isFromRepeater) => {
			if (
				this.isAlive === false
				||
				!(
					sockInfo.logVersion < tillVersion || (sockInfo.logVersion === null && tillVersion !== null)
				)
			)
			{
				clearInterval(repeater)
				repeater = null
				return
			}


			//if (isFromRepeater === true)
			//{
			//	logItemsSlice.forEach(x => {x.timesSent--}) // hack
			//}
			const logItemsSliceFiltered = (
				logItemsSlice
				.filter(x => x.timesSent < MAX_TIMES_SEND_REPEAT) // i.e. maximum MAX_TIMES_SEND_REPEAT sends of the same logItem
			)
			if (logItemsSliceFiltered.length === 0) {
				clearInterval(repeater)
				repeater = null
				return
			}
			const logItemsAsOptimizedArrays = (
				logItemsSliceFiltered
				.map(x => [
					x.logVersion,
					x.timestamp,
					x.state
				])
			)


			/*if (
				Date.now() - logItemsAsOptimizedArrays[0][1] >= socket.vdPing / 2 / 2
				&&
				Date.now() - logItemsAsOptimizedArrays[0][1] < socket.vdPing / 2 + Math.ceil(1000/FPS) * 3
			)
			{
				console.log('ok', Date.now(), socket.vdPing, logItemsAsOptimizedArrays, Date.now() - logItemsAsOptimizedArrays[0][1])
			}
			else
			{
				console.log('fl', Date.now(), socket.vdPing, logItemsAsOptimizedArrays, Date.now() - logItemsAsOptimizedArrays[0][1])
			}*/
			socket.emit(
				'logItems',
				this.id,
				//socket.vdPing,
				logItemsAsOptimizedArrays,
				false,
				(resp) => {
					if (resp !== RESP_ACK) throw 'sendLog: ack?'
					clearInterval(repeater)
					repeater = null
					if (sockInfo.logVersion < tillVersion || (sockInfo.logVersion === null && tillVersion !== null))
					{
						sockInfo.logVersion = tillVersion
						if (typeof callbackOnAck === 'function') callbackOnAck()
					}
				}
			)
			logItemsSliceFiltered.forEach(x => {x.timesSent++})
			//if (isFromRepeater === false)
			//{
			//	logItemsSliceFiltered.forEach(x => {x.timesSent++})
			//}
		}
		sendMessage(false)
		if (MAX_TIMES_SEND_REPEAT > 1) repeater = setInterval(sendMessage.bind(this, true), LOG_PUSH_LATENCY)
	}

	getLatestItem () {
		// bad code
		if (this.log.length === 0) return null
		return this.log[this.latestLogVersion] // it also includes timesSent, but redrawSessionList doesn't care
	}

	/*snapshot () {
		return {
			...this.getLatestItem()
			, logVersion: this.latestLogVersion
		}
	}*/
}

;(async function ()
{
	const appIo = io(4000, {origins: 'visiondetails.ru:443'})
	console.log('started on port 4000, origins allowed: visiondetails.ru:443 (i.e. only users of this website can connect to us)')
	//appIo.use(p2p)


	// map{uuid(string) => User}
	const uuidToUser = new Map()

	// map{user => Array<socket>}
	//const usersToSockets = new Map()

	// map{session id (which is ~6 digits) => Array<socket>}
	//const sessToSockets = new Map()
	const sessions = new Map()

	const removeSocketFromAllSessions = function (socket) {
		sessions.forEach(session => {
			session.removeSpectatorSocket(socket)
		})
		sessions.forEach(session => {
			session.removeHostSocket(socket)
		})
	}

	const allSockets = new Set()

	const propagateNewListOfAllSessionIds = function (exceptSockets)
	{
		if (exceptSockets)
		{
			if (!Array.isArray(exceptSockets))
			{
				exceptSockets = [exceptSockets]
			}
		}
		else
		{
			exceptSockets = []
		}
		const m = Array.from(sessions.keys())
		allSockets.forEach(function (s) {
			if (exceptSockets.includes(s)) return
			s.emit('listOfAllSessionIds', m)
		})
	}

	appIo.on('connection', function (socket)
	{
		// a client is with whom we speak during the *CURRENT* connection
		// a user   is with whom we speak during all connections
		allSockets.add(socket)
		const clientCookie = cookie.parse(socket.handshake.headers.cookie || '')
		const needNewUuidAssignment = !isValidUuid(clientCookie.vdHoloplayUuid)
		const userUuid = (
			needNewUuidAssignment
			? uuidv4()
			: clientCookie.vdHoloplayUuid.toLowerCase()
		)
		if (needNewUuidAssignment)
		{
			socket.emit('newUuidAssignment', userUuid)
		}


		socket.emit('listOfAllSessionIds', Array.from(sessions.keys()))

		const user = (
			uuidToUser.get(userUuid)
			||
			uuidToUser.set(userUuid, new User(userUuid)).get(userUuid)
		)
		user.socketIds.push(socket.id)

		/*socket.on('storeMyPing', function (clientDate) {
			const ret = Date.now() - clientDate
			if (ret < 10) socket.vdPing = 100
			if (ret > 500) socket.vdPing = 500
			socket.vdPing = ret
		})*/
		socket.vdPing = 100
		socket.on('xPing', function (emitTime, respond) {
			respond(emitTime)
		})
		let lastReceivedEmitTime = 0
		{
			const fn = function () {
				const emitTime = Date.now()
				socket.emit('xPing', emitTime, function (receivedEmitTime) {
					if (emitTime === receivedEmitTime && receivedEmitTime > lastReceivedEmitTime)
					{
						lastReceivedEmitTime = receivedEmitTime
						const ret = Date.now() - emitTime
						//console.log('ping: %O', ret)
						if (ret < 10) socket.vdPing = (1000 / FPS) * 2
						else if (ret > 500) socket.vdPing = 500
						else socket.vdPing = ret
					}
				})
			}
			setTimeout(fn, 1000)
			setInterval(fn, 10000)
		}

		socket.on('hostSession', (sessionId, resultForClient) => {
			if (!isValidSessionId(sessionId))
			{
				resultForClient({status: 'error', message: 'invalid session id'})
				return
			}
			/*if (session.has(sessionId))
			{
				resultForClient({status: 'error', message: 'session already exists'})
				return
			}*/
			const newSession = new Session(function () {
				sessions.delete(sessionId)
				propagateNewListOfAllSessionIds(socket) // this line and the other in callback are duplicating the effort of emit(sessionDied)
			}, sessionId, [socket])
			sessions.set(sessionId, newSession)
			resultForClient({status: 'ok'})

			propagateNewListOfAllSessionIds()
		})
		socket.on('getTrackerTime', (uid, callback) => {
			callback(uid, Date.now())
		})
		socket.on('joinSession', (sessionId, resultForClient) => {
			const session = sessions.get(sessionId)
			if (!session)
			{
				resultForClient({status: 'error', message: 'session not found'})
				return
			}
			removeSocketFromAllSessions(socket)
			session.addSpectatorSocket(socket)
			//resultForClient({status: 'ok', snapshot: session.snapshot()})
			resultForClient({status: 'ok', latestItem: session.getLatestItem()})
		})
		socket.on('leaveSession', (sessionId) => {
			const session = sessions.get(sessionId)
			if (!session) return
			session.removeSpectatorSocket(socket)
			session.removeHostSocket(socket)
		})
		//sessIds[socket.id]
		socket.on('logItems', (sessionId, msg, useMaximumSpeed, respond) => {
			const trackerTime = Date.now()
			//console.log('!socket.addLogItems:: trackerTime=%O', trackerTime)
			if (!msg)
			{
				console.log(socket)
				console.error('{{{{{{{{{{{{{{{{{{{{{')
				console.error(socket)
				console.error(socket.handshake.headers)
				console.error('sessionId={%O} msg={', sessionId)
				console.error(msg)
				console.error('}')
				console.error('}}}}}}}}}}}}}}}}}}}}}')
				respond({status: 'error', trackerTime, message: 'no msg!'})
				return
			}
			let session
			if (sessions.has(sessionId))
			{
				session = sessions.get(sessionId)
			}
			else
			{
				session = new Session(function () {
					sessions.delete(sessionId)
					propagateNewListOfAllSessionIds() // FIXME: do we need add argument `socket`?
				}, sessionId, [socket])
				sessions.set(sessionId, session)
			}
			//msg = Array.isArray(msg) ? msg : [msg]
			session.addLogItems(msg, useMaximumSpeed)
			//msg.forEach(x => session.addLogItem(x.logVersion, x.timestamp, x.state))
			// we've got the message from the session hoster, now we need to forward it to its spectators:
			//setTimeout(function () {
				/*session.emit('setState', msg, function (x) {
					//console.log('@@: %O', x)
					return {status: 'ok'}
				})*/
			//}, 50) // ping is volatile, so we have to smooth it
			respond({status: 'ok', trackerTime})
			/*const users = sessToUsers.get(sessionId) // users of this session
			for (let i = 0; i < users.length; i++)
			{
				const sockets = usersToSockets.get(users[i])
				for (let j = 0; j < sockets.length; j++)
				{
					sockets[j].emit('setState', sessionId, msg)
				}
			}*/
		})
		socket.on('disconnect', function () {
			console.log('disconnect!')
			/*usersToSockets[uuidToUser.get(clientUuid)]
				.delete(socket)*/
			/*sessToSockets.forEach((sockets, session) => {
				sockets
			})*/
			/*sessions.forEach(session => {
				if (
					session.hostSockets.size === 1
					&&
					session.hostSockets.)
			})*/
			allSockets.delete(socket)
			removeSocketFromAllSessions(socket)
		})
		socket.on('webrtc-signal', function (sessionId, data) {
			const session = sessions.get(sessionId)
			if (!session) return

			session.webrtcSignalData = data
			session.sendWebrtcSignalToAll()
		})
	})

	/*appHttp.listen(4000, function ()
	{
		console.log('listening on *:4000')
	})*/
})().catch(err => {
	console.error(err)
});
