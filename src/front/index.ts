/*!
 * vdHoloplay
 *
 * @author   Arzet Ro <arzeth0@gmail.com>
 * @license  GPL-3.0
 */


/*
Пример:
;(function (m) {//async () => {
	m = new VDH()
	m.init(
		document.querySelector('#app'),
		{
			p2p: false,// офлайн режим
			looped: false,// false = 180°, true = 360°
			fullscreen: false,// дабы спрашивало пользователя разрешение на сером экране, по которому надо просто кликнуть
			images: [
				"assets/galleries/213/r2_open_anim0000.jpg",
				"assets/galleries/213/r2_open_anim0001.{webp,jpg}",// {jpg,webp} это эквивалент
				"assets/galleries/213/r2_open_anim0002.jpg"
			]
		}
	)
	//await m.setImages([]); поменять все картинки на ...
})(m);
Если надо определённый кадр первым, то пусть у него ~ в начале будет либо setLocalState использовать.
Использовать всё в <iframe> надо, из-за стилей (потом исправлю когда-нибудь).
*/




// Дабы для Живой Воды меньше весил скрипт,
// не импортю GoTime и другие модули для режима «зритель<-артист» и компилирую так: node run build.
// Надо потом этот index.ts как-то разделить на несколько частей, дабы 2 варианта
// можно было скомпилировать: без режима «зритель<-артист» и с.

// Артист — тот, кто вертит.
// Зритель­ — тот, кто может ТОЛЬКО смотреть и выбирать другого артиста
// Сервер — посредник между ними, когда P2P недоступен. Однако, P2P не доделан. Сервер не нужен в офлайн режиме

//import io from 'socket.io'
//import io from 'socket.io-client/dist/socket.io'
//import P2P from 'socket.io-p2p'
//import io  from 'socket.io-client'
//import Peer from 'simple-peer'
//import GoTime from './GoTime.ts'
const GoTime = {now: Date.now.bind(Date), getOffset: () => 0}

//import uqserConfig from '!!yaml-loader!./app.json'
//import yaml from 'js-yaml'
//import lessFile from '../../css/1.less'
//import less from 'less'
//import autobind from 'autobind-decorator'
//import ntp from 'socket-ntp/client/ntp'
//import $ from 'jquery/dist/jquery.slim'
//import io from 'socket.io'
//import userConfig from '../../app.yaml'

import supportsWebP from 'supports-webp';
//const userConfig = {online: true, images: ['']}



// https://tc39.github.io/ecma262/#sec-array.prototype.find
if (!Array.prototype.find) {
  Object.defineProperty(Array.prototype, 'find', {
    value: function(predicate) {
      // 1. Let O be ? ToObject(this value).
      if (this == null) {
        throw TypeError('"this" is null or not defined');
      }

      var o = Object(this);

      // 2. Let len be ? ToLength(? Get(O, "length")).
      var len = o.length >>> 0;

      // 3. If IsCallable(predicate) is false, throw a TypeError exception.
      if (typeof predicate !== 'function') {
        throw TypeError('predicate must be a function');
      }

      // 4. If thisArg was supplied, let T be thisArg; else let T be undefined.
      var thisArg = arguments[1];

      // 5. Let k be 0.
      var k = 0;

      // 6. Repeat, while k < len
      while (k < len) {
        // a. Let Pk be ! ToString(k).
        // b. Let kValue be ? Get(O, Pk).
        // c. Let testResult be ToBoolean(? Call(predicate, T, « kValue, k, O »)).
        // d. If testResult is true, return kValue.
        var kValue = o[k];
        if (predicate.call(thisArg, kValue, k, o)) {
          return kValue;
        }
        // e. Increase k by 1.
        k++;
      }

      // 7. Return undefined.
      return undefined;
    },
    configurable: true,
    writable: true
  });
}

const getElementIdx = function (el: HTMLElement): number
{
	return Array.prototype.indexOf.call(
		(el.parentNode as HTMLElement).children as HTMLCollection,
		el
	)
}


const doc = document
const htmlTag = doc.documentElement
const addEventListener = window.addEventListener
const dispatchEvent = window.dispatchEvent
const newcssresize = new Event('newcssresize')
const requestAnimationFrame: Function = window.requestAnimationFrame
const cancelAnimationFrame:  Function = window.cancelAnimationFrame
const dateNow = Date.now.bind(Date)
const mathMin = Math.min.bind(Math)
const mathMax = Math.max.bind(Math)

const PROJECT_NAME: string = 'vdHoloplay'
const TIMEOUT_OVERHEAD: number = 2
const RESP_ACK = 'ACK'

let role: string = 'server'
//const $window = $(window)
//const appElement = doc.getElementById('app') as HTMLElement
//appElement.style.display = ''

let inputAccepted: boolean = false

const FPS: number = 60
const FRAME_DURATION_IN_MS_INTEGER_CEIL:  number = ~~Math.ceil( 1000 / FPS)
const FRAME_DURATION_IN_MS_INTEGER_FLOOR: number = ~~Math.floor(1000 / FPS)
const FRAME_DURATION_IN_MS_FLOAT: number = 1000 / FPS
const MAXPING: number = 500
const WAIT_FRAMES_BEFORE_SYNC: number = 3

let uuid: string = '' // `null` would get us warnings, so use only `string`

const DEV_MODE: boolean = /X11; Linux/.test(navigator.userAgent)

type SessionIdType = number
type SessionListType = {[key: number]: Session}



//;(window as any).GoTime = GoTime


type InputMethod = 'k'|'m'
type ItemAsObject = {
	readonly logVersion: number,
	readonly timestamp: number,
	readonly state: number,

	readonly ping?: number,
	readonly recv?: number,
	processedPing?: number,

	addedToFrameQueue?: boolean,
	/*inputMethod: InputMethod,*/
}
type ItemAsArray = [number, number, number]
type FrameStateType = number

const peerSettings = Object.freeze({
	stream: false,
	/*config: { iceServers: [
		//{urls: 'stun:stun.services.mozilla.com'},
		{urls: 'stun:stun.l.google.com:19302' },
	]},*/
})

const passiveListenerOptions = Object.freeze({
	capture: false,
	passive: true,
	once: false,
})


let pingToTracker: number = 110

/*if (!Peer.WEBRTC_SUPPORT) {
	alert('В браузере нет WebRTC')
}*/

const getX = (event): number => (
	(event.touches && event.touches.length)
	? /*mathMax(0, */event.touches[0].pageX//)
	: /*mathMax(0, */event.pageX//)
)
const getY = (event): number => (
	(event.touches && event.touches.length)
	? /*mathMax(0, */event.touches[0].pageY//)
	: /*mathMax(0, */event.pageY//)
)

/**
 * LMB is "the Left Mouse Button"... but we'll also assume that that might be a touch on a touchscreen
 * `inputState` is 0 when user doesn't hold LMB.
 * `inputState` is 1 when user had begun to hold LMB, but hasn't moved the cursor more than by 80px yet.
 * `inputState` is 2 when user both holds LMB and has moved the cursor more than by 80px while holding LMB.
 * `inputState` is 200 when user uses keyboards' Left and Right keys.
 * If user simultaneously uses both keyboard and mouse, then it's undefined behaviour.
 */
let inputState: 0|1|2|200 = 0

const LEFT_KEY_CODE  = 37
const RIGHT_KEY_CODE = 39
const SHIFT_KEY_CODE = 16
const CTRL_KEY_CODE = 17
const SPACEBAR_KEY_CODE = 32
const AUTOROTATE_KEY_CODE = 65 // A
const TOGGLE_LOOP_KEY_CODE = 76 // L
// R = 82




class FrameRenderer {
	//private readonly frames: {[key: number]: {[key: number]: Function}} = {}
	private readonly frames: {[key: number]: {[key: number]: [Function, FrameStateType]}} = {}
	private forceFrames: null|{readonly [key: number]: [Function, FrameStateType]} = null
	private lastAddedFrameTime: number = 0
	private lastFrameWasDisplayedAt: number = 0
	//public readonly r = {}
	private started: boolean = false
	private animationJobId: number = 0

	/*const frameGetter = (async function* frameGetter () {
		;
	})();*/

	constructor (private sessions: SessionListType){this.animate = this.animate.bind(this)}

	public addFrame (timestamp: number|null, sessionId: Session|SessionIdType, fn: ((boolean, number) => void), x: number): void {
		if (timestamp === null)
		{
			if (sessionId instanceof Session) sessionId = sessionId.id
			const nextFrameDate: number = dateNow() + FRAME_DURATION_IN_MS_INTEGER_CEIL | 0//getNextShortTimestamp()
			//console.log(this.frames)
			this.lastAddedFrameTime = mathMax(this.lastAddedFrameTime, nextFrameDate) | 0
			this.forceFrames = {[sessionId]: [fn, x]}
			//this.frames[nextFrameDate] = this.frames[nextFrameDate] || {}
			//this.frames[nextFrameDate][sessionId] = [fn, x]
			//this.r[nextFrameDate] = x
		}
		else
		{
			timestamp = timestamp | 0
			const curTimestamp = dateNow() | 0
			this.lastAddedFrameTime = mathMax(
				this.lastAddedFrameTime,
				timestamp
			)
			if (curTimestamp <= timestamp + 2) // if they are equal, then it is unknown whether the frame would be displayed at all
			{
				//console.log('addFrame: now=%O, ts=%O, state=%O', curTimestamp, timestamp, x)
				if (sessionId instanceof Session) sessionId = sessionId.id
				this.frames[timestamp] = this.frames[timestamp] || {}
				this.frames[timestamp][sessionId] = [fn, x]// = () => {console.log('animate.state='+x);fn();}
				//this.r[timestamp] = x
			}
			else
			{
				console.error('addFrame: now=%O, ts=%O, state=%O', curTimestamp, timestamp, x)
				fn(false, GoTime.now())
				return
			}
		}
	}


	// очень важная функция
	//@autobind
	private animate (pageLoadedSince: number): void {
		const frameIsShownAtTrackerDate: number = GoTime.now()
		if (this.forceFrames !== null)
		{
			this.lastFrameWasDisplayedAt = pageLoadedSince
			for (const sessionId in this.forceFrames)
			{
				const session = this.sessions[sessionId]
				const oldFrameState = session.frameState
				const [fn, newFrameState] = this.forceFrames[sessionId]
				if (oldFrameState === newFrameState)
				{
					console.error('frameRenderer.animate:: %O === %O', oldFrameState, newFrameState)
					fn(false)
					continue
				}
				//f[sessionId]()
				for (let i: number = 0; i < session.framesByContainer.length; i++)
				//for (const frameContainer of session.framesByContainer)
				{
					const frameContainer = session.framesByContainer[i]
					//;(<any>frameContainer)[newFrameState].style.willChange = 'opacity'
					frameContainer[newFrameState].className = 'anim__img anim__img_active'
					frameContainer[oldFrameState].className = 'anim__img anim__img_hidden'
					//;(<any>frameContainer)[oldFrameState].style.willChange = 'auto'
					/*frameContainer[oldFrameState]
						.classList.add('anim__img_hidden')
					frameContainer[oldFrameState]
						.classList.remove('anim__img_active')
					frameContainer[newFrameState]
						.classList.add('anim__img_active')
					frameContainer[newFrameState]
						.classList.remove('anim__img_hidden')*/
				}
				session.frameState = newFrameState
				fn(true, frameIsShownAtTrackerDate)
			}
			this.forceFrames = null
			requestAnimationFrame(this.animate)
			return
		}

		/*const timestamp = getCurrentShortTimestamp()
		if (this.frames[timestamp])*/
		for (const timestamp in this.frames)
		{
			const curTime = dateNow()
			//if (curTime + FRAME_DURATION_IN_MS_FLOAT < +timestamp)
			if (
				curTime < +timestamp
				&&
				(
					this.lastAddedFrameTime
					-
					this.lastFrameWasDisplayedAt
					> // FIXME: or >=?
					FRAME_DURATION_IN_MS_INTEGER_CEIL
				)
			)
			{
				//console.log(curTime, +timestamp)
				continue
			}

			const f = this.frames[timestamp]
			delete this.frames[timestamp]
			if (
				curTime - mathMax(FRAME_DURATION_IN_MS_INTEGER_CEIL, this.lastAddedFrameTime - this.lastFrameWasDisplayedAt) > +timestamp
			)
			{
				setTimeout(() => {
					/*if (+timestamp + FRAME_DURATION_IN_MSEC_FLOAT > curTime)
					{

					}*/
					console.warn('animate:: skip %O by %dms', timestamp, curTime - +timestamp)

					for (const sessionId in f)
					{
						const session = this.sessions[sessionId]
						const [fn, newFrameState] = f[sessionId]

						fn(false, frameIsShownAtTrackerDate)
						//session.frameState = newFrameState
					}
				}, ((FRAME_DURATION_IN_MS_FLOAT/2-1)|0))
				continue
			}
			for (const sessionId in f)
			{
				const session = this.sessions[sessionId]
				const oldFrameState = session.frameState
				const [fn, newFrameState] = f[sessionId]
				if (oldFrameState === newFrameState)
				{
					console.error('frameRenderer.animate:: %O === %O', oldFrameState, newFrameState)
					fn(false)
					continue
				}
				//f[sessionId]()
				for (let i: number = 0; i < session.framesByContainer.length; i++)
				{
					const frameContainer = session.framesByContainer[i]
					//;(<any>frameContainer)[newFrameState].style.willChange = 'opacity'
					frameContainer[newFrameState].className = 'anim__img anim__img_active'
					frameContainer[oldFrameState].className = 'anim__img anim__img_hidden'
					//;(<any>frameContainer)[oldFrameState].style.willChange = 'auto'
					/*frameContainer[oldFrameState]
						.classList.add('anim__img_hidden')
					frameContainer[oldFrameState]
						.classList.remove('anim__img_active')
					frameContainer[newFrameState]
						.classList.add('anim__img_active')
					frameContainer[newFrameState]
						.classList.remove('anim__img_hidden')*/
				}
				console.log('animate(%O), TS=%O', newFrameState, timestamp)
				session.frameState = newFrameState
				fn(true, frameIsShownAtTrackerDate)
			}
			break
		}
		requestAnimationFrame(this.animate)
	}

	public start (): void {
		if (this.started) return
		this.started = true
		this.animationJobId = requestAnimationFrame(this.animate)
	}

	public stop (): void {
		if (!this.started) return
		if (this.animationJobId !== 0)
		{
			cancelAnimationFrame(this.animationJobId)
			this.animationJobId = 0
		}
		//this.frames.length = 0
		//this.frames.splice(0, this.frames.length)
		for (const frameId in this.frames)
		{
			if (this.frames.hasOwnProperty(frameId))
			{
				delete this.frames[frameId]
			}
		}
		this.forceFrames = null
		this.lastAddedFrameTime = 0
		this.lastFrameWasDisplayedAt = 0
		this.started = false
	}

	//public clear (): void {
	//}
}


/*enum SessionMode {
	ServerOffline = 1,
	ServerOnline = 2,
	ClientOffline = 3,
	ClientOnline = 4
}*/


abstract class Session {
	alive: boolean = true
	get isClient (): boolean {
		return !this.isServerMode
		/*return (
			this.mode === SessionMode.clientOnline
			||
			this.mode === SessionMode.clientOffline
		)*/
	}
	// set isClient (x: boolean) {
	// 	this.isServerMode = !x
	// }
	readonly log: Array<ItemAsObject> = []
	private _setLocalState_locked: boolean = false
	protected temporaryStateOffset: number = 0
	activeLogVersion: null|number = null

	readonly sessionElement: HTMLElement

	readonly frameContainers: Array<HTMLElement>
	framesByContainer: Array<Array<HTMLElement>> = []

	serverFrameWidth: number = -1 // {shown jpg}'s image width

	protected framePrecision: number = -1// = 360/60
	protected readonly minFrameState: number = 0
	protected maxFrameState: number = -1
	frameState: FrameStateType = -1
	protected state2firstFrameState: number = 0


	//thesocket: any // FIXME


	constructor (
		protected thesocket: any
		, public readonly id: SessionIdType
		//, sessionElement
		, animImagesHtmlCodeInitial: DocumentFragment
		, sessionListElement: HTMLElement
		, protected readonly frameRenderer: FrameRenderer
		, public isServerMode: boolean
		//, public mode: SessionMode
		//, initialLogVersion
	) {
		;(<any>window).s = this
		doc.body.style.overflowY = 'hidden'
		this.sessionElement = doc.createElement('DIV')
		this.sessionElement.classList.add('session')
		for (
			let i: number = 1;
			i <= (
				/*(
					mode === SessionMode.serverOnline
					||
					mode === SessionMode.serverOffline
				)*/
				isServerMode
				? 1
				: 4
			);
			i++
		)
		{
			const animElement = doc.createElement('DIV')
			animElement.className = 'anim anim' + i
			animElement.appendChild(animImagesHtmlCodeInitial)
			this.sessionElement.appendChild(animElement)
		}
		sessionListElement.appendChild(this.sessionElement)
		/*{
			console.log(this.sessionElement)
			const ee = this.sessionElement.children
			const ll: number = ee.length
			for (let i: number = 0; i < ll; i++)
			{
				if (
					ee[i] instanceof HTMLElement
					&&
					ee[i].classList.contains('anim')
					&&
					ee[i].parentNode
				)
				{
					(ee[i].parentNode as HTMLElement).removeChild(ee[i])
				}
			}
		}*/
		doc.body.style.overflowY = ''

		this.frameContainers = Array.from(//[].slice.call(
			this.sessionElement.querySelectorAll('div.anim')
		)
		/*{
			const innerHTML = this.frameContainers[0].innerHTML;
			for (let i: number = 1; i < this.frameContainers.length; i++)
			{
				this.frameContainers[i].innerHTML = innerHTML;
			}
		}*/

		this.lala()


		/*this._processNewLogItemsWorker = new (Worker as any)(
			Session._processNewLogItemsWorkerCode,
			{credentials: 'same-origin'}
		)*/
	}

	protected lala ()
	{
		// в этой функции все переменные readonly должны быть вообще
		this.framesByContainer = this.frameContainers.map(frames => {
			return Array.from/*[].slice.call*/(frames.getElementsByTagName('img'))
		})
		const mainFrames = this.framesByContainer[0]

		for (const frameContainer of this.framesByContainer)
		{
			for (const frame of frameContainer)
			{
				frame.ondragstart = frame.oncontextmenu
					= () => false
			}
			Object.freeze(frameContainer)
		}
		Object.freeze(this.framesByContainer)


		this.maxFrameState  = mainFrames.length - 1
		this.framePrecision = 360 / this.maxFrameState
		this.serverFrameWidth = this.frameContainers[0].offsetWidth
		addEventListener('newcssresize', () => {
			this.serverFrameWidth = this.frameContainers[0].offsetWidth
		})
		this.frameState = getElementIdx(
			this.frameContainers[0].querySelector('.anim__img_active') as HTMLElement
		)
	}

	public updateAnimImages (animImagesHtmlCode: DocumentFragment): void
	{
		const l = this.sessionElement.children
		const ll = this.sessionElement.children.length
		for (
			let i: number = 0;
			i < ll;
			i++
		)
		{
			l[i].appendChild(
				/*i === 0 ? animImagesHtmlCode : */animImagesHtmlCode.cloneNode()
			)
		}
		this.lala()
	}

	// used only when online (one computer controls, another computer displays):
	protected netEmit (type: string, ...args): void
	{
		if (this.thesocket.p2p.readyPeers)
		{
			this.thesocket.p2p.emit('peer-' + type, ...args)
		}
		else
		{
			this.thesocket.emit(type, ...args)
		}
	}

	// used only when online (one computer controls, another computer displays):
	protected netOn (type: string, fn: Function): void
	{
		if (this.thesocket.p2p.readyPeers)
		{
			this.thesocket.p2p.on('peer-' + type, fn)
		}
		else
		{
			this.thesocket.on(type, (...args) => {
				if (!this.thesocket.p2p.readyPeers) {
					fn(...args)
				}
			})
		}
	}

	/*static readonly _processNewLogItemsWorkerCode = window.URL.createObjectURL(new Blob([`
		self.onmessage = function (event) {
			const workerPostLag = dateNow() - event.data.postedToWorkerAt
			if (workerPostLag >= event.data.timeout)
			{
				console.error(workerPostLag)
			}
			else
			{
				if (workerPostLag > 2) console.warn(workerPostLag)
				setTimeout(() => {
					self.postMessage(event.data)
				}, event.data.timeout - workerPostLag)
			}
		}
	`], {type: 'application/javascript'}));
	private readonly _processNewLogItemsWorker: Worker*/

	public getItemBeforeLogVersion (logVersion: number): ItemAsObject|null {
		if (logVersion === 0) return null
		for (let i: number = logVersion - 1; i >= 0; i--)
		{
			if (this.log[i]) return this.log[i]
		}
		return null
	}

	private _processNewLogItems_locked: boolean = false
	private _processNewLogItems_lastEnded: number = 0//Infinity
	private _processNewLogItems_pingWhenLastEnded: number = 0
	private _processNewLogItems_lastAddSmoothTime: number = 0
	// used only when online (one computer controls, another computer displays):
	protected async processNewLogItems (): Promise<void> {
		if (this._processNewLogItems_locked)
		{
			console.warn('processNewLogItems:: locked')
			return
		}
//;(window as any).ping = ping
		this._processNewLogItems_locked = true

		//if (this.log.length)
		if (this.activeLogVersion !== null && this.activeLogVersion + 1 === this.log.length)
		{
			this._processNewLogItems_locked = false
			console.warn('immediate unlock! %O %O', this.activeLogVersion, this.log.length)
			return
		}
		let firstIStart: number = 0
		for (
			firstIStart = (this.activeLogVersion === null ? 0 : this.activeLogVersion + 1);
			firstIStart < this.log.length;
			firstIStart++
		)
		{
			if (this.log[firstIStart])
			{
				break
			}
			else if (firstIStart + 1 === this.log.length)
			{
				this._processNewLogItems_locked = false
				console.error('immediate unlock! %O %O %O', this.activeLogVersion, firstIStart, this.log)
				return
			}
		}
		if (!this.log[firstIStart])
		{
			console.error('processNewLogItems:: Cannot process empty log!', this.log, this.activeLogVersion, firstIStart)
		}
		const firstTimestampInLoop: number = this.log[firstIStart].timestamp

		//const worker = this._processNewLogItemsWorker


		//let _tDiff: number = 0
		//for (let q = 0;;q++) {
			//console.log('loop iteration=%d, activeLogVersion=%O', q, this.activeLogVersion)
			const initialLogLength = this.log.length
			let lastLogVersionDoneHere: number = -1 // unused variable
			let iStart: number|void = void 0
			for (
				let i: number = (this.activeLogVersion === null ? 0 : this.activeLogVersion + 1);
				i < initialLogLength;
				i++)
			{
				if (!this.log[i] || this.log[i].addedToFrameQueue) continue
				iStart = i
				break
			}
			if (iStart === void 0) return//break
			//iStart = iStart | 0

			const previousItem = this.getItemBeforeLogVersion(iStart)

			const processedPing: number = (
				(
					!previousItem
					||
					(
						previousItem
						&&
						(
							typeof previousItem.processedPing !== 'number'
							||
							(
								typeof previousItem.recv === 'number'
								&&
								((this.log[iStart as number] as ItemAsObject).recv as number) - previousItem.recv > MAXPING
							)
						)
					)
				)
				? pingToTracker * 5
				: ((previousItem as ItemAsObject).processedPing as number)
			)
			//console.log('529: previousItem=%O, pingToTracker=%O', previousItem, pingToTracker)
			const addSmoothTime: number = Math.ceil(FRAME_DURATION_IN_MS_FLOAT * WAIT_FRAMES_BEFORE_SYNC) + processedPing//+ pingToTracker*2/*Math.ceil(FRAME_DURATION_IN_MS_FLOAT * 2) + 2 + pingToTracker*/
			/*{
				const timeDiff = dateNow() - this._processNewLogItems_lastEnded
				if (
					timeDiff
					<=
					addSmoothTime//ping//mathMax(this._processNewLogItems_pingWhenLastEnded, ping)
				)
				{
					addSmoothTime -= timeDiff
				}
			}*/


			//const shallWeContinue: boolean = (await new Promise(resolve => {
				/*worker.onmessage = (event) => {
					const {logVersion, state, timestamp, shouldBeTime} = event.data
					console.log('%O - %O = %O', GoTime.now(), shouldBeTime, GoTime.now() - shouldBeTime)
					//console.log('cmp last(%d) < logVersion(%d)', lastLogVersionDoneHere, logVersion)
					if (lastLogVersionDoneHere < logVersion)
					{
						lastLogVersionDoneHere = logVersion
						this.activeLogVersion  = logVersion
						console.log('[%d::] %d (smooth=%d) msecs late', q, GoTime.now() - this.log[logVersion].timestamp - addSmoothTime, addSmoothTime)
						//this.setLocalState(state)
						requestAnimationFrame(this.setLocalState.bind(this, state))
						//console.log('%O + 1 === %O', logVersion, initialLogLength)
						if (logVersion + 1 === initialLogLength)
						{
							(worker.onmessage as Function|null) = null
							//console.log('resolve(shallWeContinue) = %d', this.log.length > initialLogLength)
							resolve(this.log.length > initialLogLength)
						}
					}
				}*/
				//const firstTimestampInCycle: number = this.log[iStart as number].timestamp
				//let isFirstI: boolean = true

				//const _t1: number = dateNow()
				const curTimeOffset: number = GoTime.getOffset()
				for (let i = (iStart as number); i < initialLogLength; i++)
				{
					if (!this.log[i] || this.log[i].addedToFrameQueue) continue
					this.log[i].addedToFrameQueue = true
					this.log[i].processedPing = processedPing
					const curTime = GoTime.now()
					/*if (ping < 300 && curTime - this.log[i].timestamp >= 300)
					{
						if (i + 1 === initialLogLength)
						{
							resolve(this.log.length > initialLogLength)
							return
						}
						else
						{
							continue
						}
					}*/
					/*if (isFirstI && false)
					{
						this.activeLogVersion = i
						//console.log('[%O::first]%d (smooth=%d) msecs late', q, GoTime.now() - this.log[i].timestamp - addSmoothTime, addSmoothTime)
						//setTimeout(() => this.setLocalState(this.log[i].state), 0)
						//setTimeout(() => requestAnimationFrame(this.setLocalState.bind(this, this.log[i].state)), 0)
						//requestAnimationFrame(this.setLocalState.bind(this, this.log[i].state))
						console.log(
							'item%O + smooth%O,,, willBe%O vs curTime%O, frame=%O diff=%O',
							getNextShortTimestamp(), newSmoothTime, getNextShortTimestamp() + newSmoothTime, getCurrentShortTimestamp(), this.log[i].state, getNextShortTimestamp() + newSmoothTime - getCurrentShortTimestamp()
						)
						//this.frameRenderer.addFrame(getNextShortTimestamp() + newSmoothTime, this, () => {
						this.frameRenderer.addFrame(
							this.log[i].timestamp - GoTime.getOffset() + newSmoothTime,
							this,
							() => {
								this.setLocalState(this.log[i].state)
							},
							this.log[i].state
						)
						_tDiff = _tDiff + (dateNow() - _t1)
						//console.log('_tDiff = %O', _tDiff)
						isFirstI = false
						if (i + 1 === initialLogLength)
						{
							resolve(this.log.length > initialLogLength)
							return
						}
					}
					else*/
					{
						//const t2: number = this.log[i + 1].timestamp
						//const t1: number = this.log[i].timestamp
						const itemTs: number = this.log[i].timestamp - curTimeOffset
						//const timeout = itemTs - firstTimestampInLoop + (-_tDiff + addSmoothTime)//(q === 0 ? 0 : addSmoothTime)
						//const timeout = itemTs - firstTimestampInLoop + (-_tDiff + FRAME_DURATION_IN_MS_FLOAT*3)//(q === 0 ? 0 : addSmoothTime)
						/*const remainingLog = this.log.slice(i + 1, initialLogLength).filter(Boolean) // initalLogLength should still be equal to this.log.length
						if (remainingLog.length > 0 && remainingLog.length < 3)
						{
							const nextTs = remainingLog[0].timestamp
							if (nextTs - itemTs <= 5) continue
						}*/

						//console.log('item[%d] %O + smooth%O (processedPing=%O, pingToTracker=%O),,, GoTime.getOffset = %O, curTime%O vs willBe%O, frame=%O, diff=%O', i, itemTs, addSmoothTime, processedPing, pingToTracker, GoTime.getOffset(), dateNow(), itemTs + addSmoothTime, this.log[i].state, itemTs + addSmoothTime - dateNow())
						//console.log('632:: itemTs%O + addSmoothTime%O (processedPing=%O)', itemTs, addSmoothTime, processedPing)
						if (itemTs + addSmoothTime - dateNow() <= 0) {console.log(itemTs + addSmoothTime - dateNow())}
						this.frameRenderer.addFrame(itemTs + addSmoothTime, this, (isOk) => {
						//this.frameRenderer.addFrame(GoTime.now() + timeout, this, () => {
							if (lastLogVersionDoneHere < i)
							{
								lastLogVersionDoneHere = i
								this.activeLogVersion  = i
								//console.log('[%d::] %d (smooth=%d) msecs late', q, GoTime.now() - this.log[i].timestamp - addSmoothTime, addSmoothTime)
								//this.setLocalState(this.log[i].state)
								if (i + 1 === initialLogLength)
								{
									if (!isOk && this.log.length === initialLogLength)
									{
										//this.setLocalState(this.log[i].state)
									}
									//resolve(this.log.length > initialLogLength)
								}
							}
						}, this.log[i].state)
						/*worker.postMessage({
							postedToWorkerAt: dateNow(),
							timeout: timeout,
							shouldBeTime: GoTime.now() + timeout,
							//timeout: t2-t1,
							logVersion: i,
							state: this.log[i].state
						})*/
					}
				}
				//resolve(this.log.length > initialLogLength)
				//resolve(true)
				//return
			//})) as boolean
			//console.log('gonna terminate: %O', !shallWeContinue)
			//if (!shallWeContinue) break
		//}

		//worker.terminate()


		//console.log('unlock')
		this._processNewLogItems_lastEnded = dateNow()
		//this._processNewLogItems_pingWhenLastEnded = ping
		this._processNewLogItems_lastAddSmoothTime = addSmoothTime
		this._processNewLogItems_locked = false
	}

	/**
	 * @param  {number} digits How many digits
	 * @return {number}
	 */
	// used only when online (one computer controls, another computer displays):
	static genSessionId (digits: number = 5): SessionIdType {
		const min: number = Math.pow(10, digits - 1)
		const max: number = Math.pow(10, digits)
		// if 1e5 is returned, then with 50% there will be one collision per  372 generated sessionIds
		// if 1e6 is returned, then with 50% there will be one collision per 1177 generated sessionIds
		// if 1e7 is returned, then with 50% there will be one collision per 3723 generated sessionIds
		let ret: number
		do {
			ret = Math.round(
				Math.random() * (max - min) + min
			)
		} while (/(666|^13|13$)/.test(ret.toString()))
		return ret
	}

	/**
	 * @return {boolean} Whether something has been indeed added by this function right now
	 */
	// used only when online (one computer controls, another computer displays):
	protected addRemoteLogItems (/*fromVersion: number, tillVersion: number, */newItems: Array<ItemAsArray>): boolean {
		if (!Array.isArray(newItems)) throw new Error('addRemoteLogItems:: Not an array: ' + newItems)
		//console.log('addRemoteLogItems (fromVersion=%O, tillVersion=%O, newItems=%O)', fromVersion, tillVersion, newItems)
		//newItems = newItems || []
		const logLength = this.log.length
		//if (fromVersion === void 0) fromVersion = logLength
		//const tillVersion = fromVersion + newItems.length
		/*if (fromVersion < logLength)
		{
			if (tillVersion < logLength) return false
			//fromVersion = logLength
		}*/

		const _p: number = pingToTracker
		let addedSomething: boolean = false

		for (let i: number = 0; i < newItems.length; i++)
		{
			//console.log('this.log[%d] = %O', fromVersion + i, newItems[i])
			if (!this.log[newItems[i][0]/*fromVersion + i*/])
			{
				addedSomething = true
				this.log[newItems[i][0]/*fromVersion + i*/] = {
					logVersion: newItems[i][0],
					timestamp:  newItems[i][1],
					state:      newItems[i][2],

					ping: _p,
					addedToFrameQueue: false,
					recv: GoTime.now()
				}
			}
		}

		return addedSomething
	}

	// функция нигде не используется в самой программе, кроме как
	// единожды установить самый первый кадр когда сервер посылает
	// свой первый список кадров.
	// Но пользователь её может использовать:
	// setLocalState(30) сделает так, чтобы 30-й кадр показывался.
	public setLocalState (newFrameState: FrameStateType, smooth: boolean = false): void {
		/*else
		{
			console.log('setLocaleState: %O', newFrameState);
		}*/
		if (this._setLocalState_locked)
		{
			console.warn('setLocalState:: we are locked!!! failed to change from %O to %O', this.frameState, newFrameState)
			return
		}
		this._setLocalState_locked = true



		const oldFrameState: FrameStateType = this.frameState
		/*if (!(inputState === 200 && Math.abs(oldFrameState - newFrameState) === 2)) // BUG: it warns when keyboard+(0=>58)
		{
			if (newFrameState === 0 && oldFrameState === this.maxFrameState) {}
			else if (newFrameState === this.maxFrameState && oldFrameState === 0) {}
			else if (Math.abs(newFrameState - oldFrameState) > 1)
			{
				console.warn('setLocalState: %d => %d', oldFrameState, newFrameState)
			}
		}*/
		/*else
		{
			console.log('setLocalState: %d => %d', oldFrameState, newFrameState)
		}*/
		/*if (smooth)
		{
			newFrameState = (newFrameState + this.temporaryStateOffset) % (this.maxFrameState + 1)
			if (newFrameState < 0) newFrameState = this.maxFrameState + 1 - Math.abs(newFrameState)

			if (!(inputState === 200 && Math.abs(oldFrameState - newFrameState) === 2))
			{
				if (newFrameState === 0 && oldFrameState === this.maxFrameState) {}
				else if (newFrameState === this.maxFrameState && oldFrameState === 0) {}
				else if (newFrameState > oldFrameState + 1) // [0..57]=>59
				{
					const oldNewFrameState = newFrameState
					if (newFrameState > oldFrameState + (this.maxFrameState / 2)) // 3=>59
					{
						newFrameState = (oldFrameState - 1) % (this.maxFrameState + 1)
						this.temporaryStateOffset--
					}
					else { // 57=>59
						newFrameState = (oldFrameState + 1) % (this.maxFrameState + 1)
						this.temporaryStateOffset++
					}
					if (newFrameState < 0) newFrameState = this.maxFrameState + 1 - Math.abs(newFrameState)
					console.warn('[%d] setLocaleState: %O => %O, fixing to %O', dateNow(), oldFrameState, oldNewFrameState, newFrameState);
				}
				else if (newFrameState < oldFrameState - 1) // [2..59] => 0
				{
					const oldNewFrameState = newFrameState
					if (newFrameState < oldFrameState - (this.maxFrameState / 2)) // 59=>3
					{
						newFrameState = (oldFrameState + 1) % (this.maxFrameState + 1)
						this.temporaryStateOffset++
					}
					else // 59=>57
					{
						newFrameState = (oldFrameState - 1) % (this.maxFrameState + 1)
						this.temporaryStateOffset--
					}
					if (newFrameState < 0) newFrameState = this.maxFrameState + 1 - Math.abs(newFrameState)
					console.warn('[%d] setLocaleState: %O => %O, fixing to %O', dateNow(), oldFrameState, oldNewFrameState, newFrameState);
				}
			}
		}*/

		if (oldFrameState !== newFrameState)
		{
			for (let i = 0; i < this.framesByContainer.length; i++)
			//for (const frameContainer of this.framesByContainer)
			{
				const frameContainer = this.framesByContainer[i]
				//;(<any>frameContainer)[newFrameState].style.willChange = 'opacity'
				frameContainer[newFrameState].className = 'anim__img anim__img_active'
				frameContainer[oldFrameState].className = 'anim__img anim__img_hidden'
				//;(<any>frameContainer)[oldFrameState].style.willChange = 'auto'
				/*frameContainer[oldFrameState]
					.classList.add('anim__img_hidden')
				frameContainer[oldFrameState]
					.classList.remove('anim__img_active')
				frameContainer[newFrameState]
					.classList.add('anim__img_active')
				frameContainer[newFrameState]
					.classList.remove('anim__img_hidden')*/
			}
			this.frameState = newFrameState
		}
		this._setLocalState_locked = false
	}
}


type QueueGoodItem = {idx: number, timesSent: number, message: ItemAsArray}
class artistSession extends Session {
	private firstX: number = 0 // what was the mouse position at X axis at mousedown/touchstart
	private firstY: number = 0 // what was the mouse position at Y axis at mousedown/touchstart
	readonly queue: Array<null|QueueGoodItem> = []
	//fromTrackerToUsDelayTime: number = 100
	protected speed: number = WAIT_FRAMES_BEFORE_SYNC // the less the faster, min is 1?

	constructor (
		thesocket
		, public readonly id: SessionIdType
		/*, sessionElement*/
		, animImagesHtmlCode: DocumentFragment
		, sessionListElement: HTMLElement
		, protected looped: boolean
		, protected autoRotationEnabled: boolean
		, protected readonly frameRenderer: FrameRenderer
	)
	{
		super(thesocket, id/*, sessionElement*/, animImagesHtmlCode, sessionListElement, frameRenderer, true)

		let state2isInProgress: boolean = false // крутит ли пользователь (то есть mousedown/touchstart уже наступил)
		{
			let ctrlPressed:  boolean = false
			let shiftPressed: boolean = false
			let direction: -1|1 = -1 // -1 is right, 1 is left
			let interval: number = 0

			if (thesocket) inputAccepted = true

			const intervalFn = (): void => {
				// Uses outside variables: ctrlPressed, shiftPressed, direction
				//console.log('intervalFn anda, inputState=%O!', inputState)
				if (inputState !== 0 && inputState !== 200) return // нельзя автоповорот и ручной поворот одновременно
				if (state2isInProgress) return
				this.state2firstFrameState = this.frameState
				const by: number = (1 / this.maxFrameState) * (shiftPressed ? 2 : 1)
				if (
					this.autoRotationEnabled
					&&
					!this.looped
					&&
					(
						this.frameState === this.maxFrameState
						||
						this.frameState === this.minFrameState
					)
				)
				{
					direction *= -1
				}
				this.seekBy(by * direction, 'k')
			}
			const startInterval = (): void =>
			{
				if (interval === 0)
				{
					interval = setInterval(
						intervalFn,
						Math.ceil(FRAME_DURATION_IN_MS_FLOAT * this.speed)
					)
				}
			}
			const stopInterval = (): void =>
			{
				if (interval !== 0)
				{
					//console.log('intervalFn se terminó, inputState=%O!', inputState)
					clearInterval(interval)
					interval = 0
				}
			}
			const restartInterval = (): void =>
			{
				stopInterval()
				startInterval()
			}
			if (autoRotationEnabled) startInterval()
			addEventListener('keydown', (event) => {
				if (role === 'client') return // TODO: does the event really persist after switching to the Client role?
				if (!inputAccepted) return
				//console.log('keydown: %O', event.keyCode)
				if (event.keyCode === SHIFT_KEY_CODE) // Shift
				{
					shiftPressed = true
					return
				}
				if (event.keyCode === CTRL_KEY_CODE) // Shift
				{
					ctrlPressed = true
					return
				}
				if (inputState === 0)
				{
					if ((<any>event)/*.originalEvent*/.shiftKey)
					{
						shiftPressed = true
					}
					if ((<any>event)/*.originalEvent*/.ctrlKey)
					{
						ctrlPressed = true
					}
					if (
						this.autoRotationEnabled === true
						&&
						(
							event.keyCode === LEFT_KEY_CODE
							||
							event.keyCode === RIGHT_KEY_CODE
						)
					)
					{
						direction = event.keyCode === LEFT_KEY_CODE ? 1 : -1
					}
					else if (
						this.autoRotationEnabled === false
						&&
						(
							event.keyCode === LEFT_KEY_CODE
							||
							event.keyCode === RIGHT_KEY_CODE
						)
					)
					{
						inputState = 200
						this.temporaryStateOffset = 0
						direction = event.keyCode === LEFT_KEY_CODE ? 1 : -1

						
						//(window as any).xxx = 5
						/*setTimeout(() => {
							if (interval === 0 && inputState === 200)
							{*/
								startInterval()
								//console.log('intervalFn se ha iniciado por `%O`, inputState=%O!', event.key, inputState)
							/*}
						}, Math.ceil(FRAME_DURATION_IN_MS_FLOAT * 3))*/
						intervalFn()
					}
				}
			}, passiveListenerOptions)
			//.on('keyup', (event) => {
			addEventListener('keyup', (event) => {
				if (role === 'client') return // TODO: does the event really persist after switching to the Client role?
				if (!inputAccepted) return
				/*console.log(
					'keyup: key=%O code=%O shiftPressed=%O inputState=%O',
					event.key,
					event.keyCode,
					shiftPressed,
					inputState
				)*/
				let lastTimeAutorotateChanged: number = 0
				let lastTimeLoopedChanged: number = 0
				if (event.keyCode === SHIFT_KEY_CODE) // Shift
				{
					shiftPressed = false
				}
				else if (event.keyCode === CTRL_KEY_CODE) // Shift
				{
					ctrlPressed = false
				}
				else if (event.keyCode === LEFT_KEY_CODE || event.keyCode === RIGHT_KEY_CODE) // Left or Right
				{
					if (this.autoRotationEnabled)
					{
						//if (event.keyCode === LEFT_KEY_CODE)  direction = -1
						//if (event.keyCode === RIGHT_KEY_CODE) direction = 1
					}
					else
					{
						inputState = 0

						stopInterval()
						

						this.temporaryStateOffset = 0
						if (this.thesocket) this.immediatelySendTheQueue()
					}
				}
				else if (
					(event.code && event.code === 'KeyL')
					||
					(!event.code && event.keyCode === TOGGLE_LOOP_KEY_CODE)
				)
				{
					if (dateNow() - lastTimeLoopedChanged < 700) return
					lastTimeLoopedChanged = dateNow()
					if (this.looped)
					{
						this.looped = false
					}
					else
					{
						this.looped = true
					}
				}
				else if (
					(event.code && ['Minus', 'NumpadSubtract'].indexOf(event.code) !== -1)
					||
					(!event.code && [173, 109].indexOf(event.keyCode) !== -1)
				) // -, numpad-
				{
					//console.log('------------')
					if (this.speed < 36)
					{
						this.speed++
						console.log('Nueva velocidad (más lento): %O', this.speed)
						if (interval !== 0) restartInterval()
					}
					else
					{
						console.warn('Ya la mínima velocidad!')
					}
				}
				else if (
					(event.code && ['Equal', 'NumpadAdd'].indexOf(event.code) !== -1)
					||
					(!event.code && [61, 107].indexOf(event.keyCode) !== -1)
				) // =, numpad+
				{
					//console.log('++++++')
					if (this.speed > 1)
					{
						this.speed--
						console.log('Nueva velocidad (más veloz): %O', this.speed)
						if (interval !== 0) restartInterval()
					}
					else
					{
						console.warn('Ya la maxima velocidad!')
					}
				}
				else if (
					(event.code && event.code === 'KeyA')
					||
					(!event.code && event.keyCode === AUTOROTATE_KEY_CODE)
				)
				{
					//console.log('AUTOROTATE_KEY_CODE pressed, this.autoRotationEnabled=%O, shiftPressed=%O', this.autoRotationEnabled, shiftPressed)
					if (dateNow() - lastTimeAutorotateChanged < 700)
					{

					}
					else if (this.autoRotationEnabled)
					{
						//if (shiftPressed || ctrlPressed)
						{
							stopInterval()
							lastTimeAutorotateChanged = dateNow()
							this.autoRotationEnabled = false
						}
					}
					else// if (!(shiftPressed || ctrlPressed))
					{
						//if (dateNow() - lastTimeAutorotateChanged > 700)
						{
							this.autoRotationEnabled = true
							startInterval()
							//console.log('intervalFn se ha iniciado por `%O`, inputState=%O!', event.key, inputState)
						}
					}
				}
			}, passiveListenerOptions)
		}

		{
			const fn = () => {
				if (role === 'client') return // TODO: does the event really persist after switching to the Client role?
				inputState = 0
				this.temporaryStateOffset = 0
				this.firstX = 0
				this.firstY = 0
				this.sessionElement.style.cursor = ''
				if (this.thesocket && !this.autoRotationEnabled)
				{
					this.immediatelySendTheQueue()
				}
				//console.log('inputState = 0')
			}
			addEventListener('mouseup',  fn, passiveListenerOptions)
			addEventListener('touchend', fn, passiveListenerOptions)
		}

		{
			const fn = (event) => {
				if (role === 'client') return // TODO: does the event really persist after switching to the Client role?
				if (inputState !== 0) return
				this.temporaryStateOffset = 0
				this.firstX = getX(event)
				this.firstY = getY(event)
				inputState = 1
				this.sessionElement.style.cursor = 'move'
				//console.log('inputState = 1')
			}
			this.sessionElement.querySelector('.anim1')!.addEventListener('mousedown',  fn, passiveListenerOptions)
			this.sessionElement.querySelector('.anim1')!.addEventListener('touchstart', fn, passiveListenerOptions)
		}
		{

			// how much px should be `mousemove`d before showing the next frame
			let oneFramePseudoWidth: number = this.serverFrameWidth / this.maxFrameState

			let lastX: number = 0

			//const shortTimestampsUsed: {[key: number]: true} = {}
			// The most important code:
			const lambda = async (event) => {
				if (state2isInProgress) return
				//if (inputState)console.log('%O, %O', inputState, state2isInProgress)
				//console.log(event)
				//MouseEvent
				//TouchEvent
				if (inputState === 1)
				{
					//console.log(getX(event), event.touches[0])
					//console.log('{1} %d - %d', getX(event), this.firstX)
					if (Math.abs(this.firstX - getX(event)) >= this.serverFrameWidth*0.04//80
					 || Math.abs(this.firstY - getY(event)) >= 80)
					{
						this.state2firstFrameState = this.frameState
						this.temporaryStateOffset = 0
						this.firstX = getX(event)
						this.firstY = getY(event)
						lastX = this.firstX
						oneFramePseudoWidth = this.serverFrameWidth / this.maxFrameState
						/*console.log(
							'oneFramePseudoWidth = %O / %O = %Opx'
							, this.serverFrameWidth
							, this.maxFrameState
							, this.serverFrameWidth / this.maxFrameState
						)*/
						inputState = 2
						//console.log('inputState = 2')
					}
				}
				else if (inputState === 2)
				{
					state2isInProgress = true
					//shortTimestampsUsed[ge]
					/*console.log(
`maxFrameState = ${this.maxFrameState}
serverFrameWidth = ${this.serverFrameWidth}
this.frameState = ${this.frameState}
this.state2firstFrameState = ${this.state2firstFrameState}
getX(event) - this.firstX = ${getX(event) - this.firstX}
`
					)*/
					//console.log('{2} %d - %d', getX(event), this.firstX)
					const newX: number = getX(event)
					const compensator = (
						(
							typeof TouchEvent !== 'undefined'
							&&
							event instanceof TouchEvent
						)
						? 1
						: 1/3
					)
					const allowToSkipFrames = (
						(
							typeof TouchEvent !== 'undefined'
							&&
							event instanceof TouchEvent
						)
						? 3
						: 3
					)

					// BTW: mousemove is not fired every nanosecond.
					// Therefore, there can be even 300px difference between two firings!
					if (
						(!allowToSkipFrames
						&&
						Math.abs(newX - lastX) >= Math.round(oneFramePseudoWidth*compensator)
						)
						||
						(allowToSkipFrames
						&&
						Math.abs(newX - lastX) >= allowToSkipFrames*Math.round(oneFramePseudoWidth*compensator)
						)
					) // we should not skip frames!
					{
						// If there was a sudden jump,
						// then let's imagine it was not.
						this.firstX = this.firstX + (newX - lastX) - Math.sign(newX - lastX) * allowToSkipFrames * Math.round(oneFramePseudoWidth*compensator)
					}
					lastX = newX// + (newX - lastX)

					{
						const __ee = Math.abs((lastX - this.firstX) / this.serverFrameWidth)
						if (__ee > 1)
						{
							//console.error('Impossible: lastX = %O, this.firstX = %O', lastX, this.firstX)
						}
					}
					/*console.log(
						'seeking by [(%O-%O) / %O] = [%O / %O] = %O',
						lastX, this.firstX,
						Math.round(this.serverFrameWidth*compensator),
						lastX - this.firstX,
						Math.round(this.serverFrameWidth*compensator),
						(lastX - this.firstX) / Math.round(this.serverFrameWidth*compensator)
					)*/
					await this.seekBy((lastX - this.firstX) / this.serverFrameWidth / compensator, 'm')
					//console.log(`result = ${this.frameState}`)
					//console.log('X=%O, Y=%O', getX(event), getY(event))
					state2isInProgress = false
				}
			}

			//let lastPageX: number = 0
			;(() => {
				const fn = (event) =>
				{
					if (role !== 'client') lambda(event) // TODO: does the event really persist after switching to the Client role?
				}
				addEventListener('mousemove', fn, passiveListenerOptions)
				addEventListener('touchmove', fn, passiveListenerOptions)
			})();
		}




		if (this.thesocket)
		{
			this.thesocket.p2p = new Peer({
				initiator: true,
				//channelName: 'vdHoloplay-' + id,
				/*offerConstraints: { 
					mandatory: { 
						OfferToReceiveAudio: true, 
						OfferToReceiveVideo: true 
					} 
				},*/
				...peerSettings,
			})
			//window.peerS = this.thesocket.p2p
			this.thesocket.on('webrtc-signal', (sessionId: SessionIdType, data: any) => {
				console.log('@@@@@@@@@@@@ %O %O', sessionId, data)
				//if (id !== sessionId) return
				console.error('webrtc-signal %O %O', sessionId, data)
				console.log(this.thesocket.p2p.signal(data))
				//this.thesocket.p2p.signal(data)
			})
			this.thesocket.p2p.on('signal', (data) => {
				console.log('peer:: signal.data: %O', data)
				this.thesocket.emit('webrtc-signal', this.id, data)
			})
			this.thesocket.p2p.on('connect', (data) => {
				console.log('peer:: connect: %O', data)
			})
			this.thesocket.p2p.on('data', (data) => {
				console.log('peer:: data.data: %O', data)
			})
			this.thesocket.p2p.on('error', (error) => {
				console.log('peer:: error.error: %O', error)
			})

			this._enableAutomaticQueueSending()
		}
	}

	// used only when online (one computer controls, another computer displays):
	private _enableAutomaticQueueSending (): void {
		this._interval = setInterval(() => {
			//if (role === 'server')
			//{
				this.sendQueue()
			//}
		}, Math.ceil(FRAME_DURATION_IN_MS_FLOAT * WAIT_FRAMES_BEFORE_SYNC))//mathMin(100, GoTime.getPrecision()))/*30/*fromTrackerToUsDelayTime + mathMin(100, GoTime.getPrecision()));*/
	}
	// used only when online (one computer controls, another computer displays):
	private _disableAutomaticQueueSending (): void {
		clearInterval(this._interval as number)
		this._interval = null
	}

	// used only when online (one computer controls, another computer displays):
	private immediatelySendTheQueue (): void {
		if (!this.thesocket) return
		if (role === 'server')
		{
			this._disableAutomaticQueueSending()
			this.sendQueue(true)
			this._enableAutomaticQueueSending()
		}
	}

	private _interval: null|number = null

	private _addNewItemToLog_locked: boolean = false
	// used only when online (one computer controls, another computer displays):
	addNewItemToLog (timestamp: number, state: FrameStateType/*, inputMethod*/): void {
		if (this._addNewItemToLog_locked === true)
		{
			console.error('addNewItemToLog: locked!')
			return
		}


		this._addNewItemToLog_locked = true
		let newItemLogVersion: number = this.log.length
		let queueLength: number = this.queue.length
		//const timestamp = GoTime.now()

		// if someone already has this timestamp, then overwrite it
		if (newItemLogVersion > 0)
		{
			if (state === this.log[newItemLogVersion - 1].state)
			{
				console.warn('addNewItemToLog: same state!')
				this._addNewItemToLog_locked = false
				return
			}
			if (timestamp === this.log[newItemLogVersion - 1].timestamp)
			{
				--newItemLogVersion
				--queueLength

				if (state === this.log[newItemLogVersion].state)
				{
					console.warn('addNewItemToLog: same state!!')
					this._addNewItemToLog_locked = false
					return
				}
				else if (newItemLogVersion >= 1 && state === this.log[newItemLogVersion - 1].state)
				{
					console.error('addNewItemToLog: same state, del!!!')
					this.queue.splice(queueLength, 1)
					this.log.splice(newItemLogVersion, 1)
					this._addNewItemToLog_locked = false
					return
				}
			}
		}


		this.log[newItemLogVersion] = {
			logVersion: newItemLogVersion,
			timestamp,
			state/*, inputMethod*/
		}
		this.queue[queueLength] = {
			idx: queueLength,
			timesSent: 0,
			message: [newItemLogVersion, timestamp, state]//this.log[newItemLogVersion] // a message that is going to be sent to the tracker (or directly to the client in case of WebRTC)
		}
		this._addNewItemToLog_locked = false
	}

	private _sendQueue_locked: boolean = false
	// used only when online (one computer controls, another computer displays):
	private sendQueue (useMaximumSpeed: boolean = false): void {
		if (this._sendQueue_locked)
		{
			//console.warn('sendQueue:: locked!')
			return
		}
		if (!this.queue)
		{
			console.error('sendQueue: sessionId `' + this.id + '` does not exist')
			this._sendQueue_locked = false
			return
		}
		this._sendQueue_locked = true
		const currentTimestamp: number = GoTime.now()
		const MAX_TIMES_SEND_REPEAT = 1
		const notSentList: Array<QueueGoodItem> = []
		const notSentListMessages: Array<ItemAsArray> = []
		for (let i: number = 0; i < this.queue.length; i++)
		{
			if (
				this.queue[i] !== null
				&&
				(this.queue[i] as QueueGoodItem).timesSent < MAX_TIMES_SEND_REPEAT
				&&
				currentTimestamp > (this.queue[i] as QueueGoodItem).message[1]
			)
			{
				notSentList.push(this.queue[i] as QueueGoodItem)
				;(this.queue[i] as QueueGoodItem).timesSent++
				notSentListMessages.push((this.queue[i] as QueueGoodItem).message)
				if ((this.queue[i] as QueueGoodItem).timesSent === MAX_TIMES_SEND_REPEAT)
				{
					this.queue[i] = null
				}
			}
		}
		//this.queue.filter(x => x.timesSent < 2) // i.e. maximum 2 sends of the same logItem
		if (notSentList.length === 0)
		{
			this._sendQueue_locked = false
			return
		}
		//const emitTime = GoTime.now()
		//const emitQime = dateNow()
		//console.log('sending queue at GoTime=%O items=%O', emitTime, JSON.stringify(notSentListMessages))
		//if (this.thesocket.p2p.readyPeers)
		this.netEmit(
			'logItems'
			, this.id
			, notSentListMessages
			, useMaximumSpeed
			, (response) => {
				//this.fromTrackerToUsDelayTime = response.trackerTime - emitTime
				/*console.warn('Qping=%O', dateNow() - emitQime)
				console.log('sendQueue.response: %O', response)
				console.group('fromTrackerToUsDelayTime=' + this.fromTrackerToUsDelayTime)
				console.log('%O should be later than', response.trackerTime)
				console.log('%O', emitTime)
				console.log('GoTime.getPrecision=%O', GoTime.getPrecision())
				console.groupEnd('fromTrackerToUsDelayTime=' + this.fromTrackerToUsDelayTime)*/
				if (response.status === 'ok')
				{
					for (let i: number = 0; i < notSentList.length; i++)
					{
						this.queue[notSentList[i].idx] = null
					}
				}
				else
				{
					console.error('wtf42353252352, response=%O', response)
				}
			}
		);
		/*for (let i = 0; i < notSentList.length; i++)
		{
			notSentList[i].timesSent++
		}*/
		this._sendQueue_locked = false
	}
	/*clearQueue () {
		if (!this.queue)
		{
			console.error('clearQueue: sessionId `' + this.id + '` does not exist')
			return
		}
		this.queue.splice(0, this.queue.length)
	}*/

	public async seekBy (timePercentPosition: number, inputMethod: InputMethod): Promise<void> {
		// на сколько процентров прокрутить
		// если кадров 60, state2firstFrameState = 60,
		// то seekBy(0.5) сделает state2firstFrameState=30
		// а если ещё раз seekBy(0.5), то будет 0.
		// Я проверял точно. seekBy(0.99) сделал бы из 60 именно 1.
		//window.seekBy = this.seekBy.bind(this)
		//console.log(this.state2firstFrameState)
		/*if (DEV_MODE)
		{
			console.assert(-1 <= timePercentPosition, `seekBy(${timePercentPosition}):: Must be >=-1, <=1, !=0`)
			console.assert(timePercentPosition <= 1, `seekBy(${timePercentPosition}):: Must be >=-1, <=1, !=0`)
			console.assert(timePercentPosition !== 0, `seekBy(0):: Must be >-1, <1, !=0!`)
		}
		else */
		if (timePercentPosition === 0) return
		/*if (
			this.looped
			&& (
				-1 > timePercentPosition
				||
				timePercentPosition > 1
			)
		)
		{
			return
		}*/
		//console.log('seekBy: %O', timePercentPosition)
		const lastFrameState: FrameStateType = this.frameState
		let newFrameState: FrameStateType = (
			this.looped
			? Math.round(
				(
					this.state2firstFrameState
					//+ this.temporaryStateOffset
					+ timePercentPosition * this.maxFrameState  /// 10
				)
				% (this.maxFrameState + 1)
			) % (this.maxFrameState + 1)
			: mathMin(this.maxFrameState,
				mathMax(0, Math.round(
					(
						this.state2firstFrameState
						//+ this.temporaryStateOffset
						+ timePercentPosition * this.maxFrameState  /// 10
					)
				))
			)
		)
		//console.warn(newFrameState)
		if (!Number.isFinite(newFrameState))
		{
			console.error('seekBy: new frame was asked to be #infinite: %d', newFrameState)
			newFrameState = 0
		}
		else if (newFrameState < 0 && this.looped)
		{
			newFrameState
				= this.maxFrameState + 1 - Math.abs(newFrameState)
		}

		//console.warn(newFrameState)
		if (newFrameState !== lastFrameState)
		{
			//console.log('FRAME: %O ⇒ %O', lastFrameState, newFrameState)
			await new Promise(resolve => {
				//const nextShortTimestamp = getNextShortTimestamp()
				//console.log('658')

				// the time still might be the same!
				this.frameRenderer.addFrame(null, this, (isOk, finalTimestamp) => {
					/*if (isOk) console.log('^SUCCESSframe=%O', finalTimestamp)
					else console.error('^FAILframe=%O', finalTimestamp)*/
					if (isOk && this instanceof artistSession/* && newFrameState !== lastFrameState*/)
					{
						this.addNewItemToLog(finalTimestamp, newFrameState/*, inputMethod*/)
					}
					//this.setLocalState(newFrameState, true)
					resolve()
				}, newFrameState)
			})
		}
		//else console.log('FRAME: %O ⇒ no hay cambios', newFrameState)
	}
}

// used only when online (one computer controls, another computer displays):
class spectatorSession extends Session {
	constructor (
		thesocket
		, id: SessionIdType
		/*, sessionElement*/
		, animImagesHtmlCode: DocumentFragment
		, sessionListElement: HTMLElement
		, protected readonly frameRenderer: FrameRenderer
	)
	{
		super(thesocket, id/*, sessionElement*/, animImagesHtmlCode, sessionListElement, frameRenderer, false)

		//thesocket.p2p.on(
		this.netOn(
			'logItems',
			(
				sessionId: SessionIdType,
				//ping: number,
				items: Array<ItemAsArray>,
				useMaximumSpeed: boolean,
				response: Function
			) => {
				//console.log('!socket.logItems: Date%O GoDate%O: ', dateNow(), GoTime.now(), sessionId, JSON.stringify(items), useMaximumSpeed)
				if (this.id !== sessionId) return
				//setTimeout(() => response(RESP_ACK), 2)
				if (this.addRemoteLogItems(/*fromVersion, tillVersion, */items))
				{
					this.processNewLogItems()
					//setTimeout(this.processNewLogItems.bind(this, ping), 1000)
				}
			}
		)

		this.thesocket.p2p = new Peer({
			initiator: false,
			//channelName: 'vdHoloplay-' + id,
			/*answerConstraints: { 
				mandatory: { 
					OfferToReceiveAudio: false, 
					OfferToReceiveVideo: false 
				} 
			},*/
			...peerSettings,
		})
		//window.peerC = this.thesocket.p2p
		this.thesocket.on('webrtc-signal', (sessionId: SessionIdType, data: any) => {
			console.log('@@@@@@@@@@@@ %O %O', sessionId, data)
			//if (id !== sessionId) return
			console.error('webrtc-signal %O %O', sessionId, data)
			console.log(this.thesocket.p2p.signal(data))
			//this.thesocket.p2p.signal(data)
		})
		this.thesocket.p2p.on('signal', (data) => {
			console.log('peer:: signal.data: %O', data)
			this.thesocket.emit('webrtc-signal', this.id, data)
		})
		this.thesocket.p2p.on('connect', (data) => {
			console.log('peer:: connect: %O', data)
		})
		this.thesocket.p2p.on('data', (data) => {
			console.log('peer:: data.data: %O', data)
		})
		this.thesocket.p2p.on('error', (error) => {
			console.log('peer:: error.error: %O', error)
		})
	}
}


class VDH
{
//try {
	/*const userConfig: {
		online: boolean,
		images: Array<string>
	} = yaml.safeLoad(rawconfig)*/
	private hasInitEverBeenStarted: boolean = false
	protected supportsWebp: boolean = false

	protected images: Array<string> = [] // можно два варианта (webp, jpg), а можно просто jpg
	protected imageWidth:  number = 0
	protected imageHeight: number = 0

	protected appElement: HTMLElement = document.body // инициализируется в .init()

	private sessions: SessionListType = {}
	private frameRenderer: FrameRenderer = new FrameRenderer(this.sessions)


	private fixNewImagesArray (images: Array<string>): void
	{
		if (!images.find(x => x[0] === '~'))
		{
			const middle = Math.round(images.length / 2)
			images[middle] = '~' + images[middle]
		}

		for (let i = 0; i < images.length; i++)
		{
			images[i] =
				images[i]
				.replace(/\{(webp),\s*(png|jpe?g)\}/i, this.supportsWebp ? '$1' : '$2')
				.replace(/\{(png|jpe?g),\s*(webp)\}/i, this.supportsWebp ? '$2' : '$1')
				+ '?3'
		}
	}
	private async prepareForNewImageSizes (): Promise<void>
	{
		const [a, b] = await new Promise(resolve => {
			const img = new Image()

			img.onload = function () {
				resolve([
					((<any>this).width as number),
					((<any>this).height as number)
				])
			}
			img.onerror = function (e) {
				console.error(e)
			}
			img.src = this.images[0].replace(/^~/, '')
		})
		this.imageWidth  = a
		this.imageHeight = b
	}
	async init (appElement: HTMLElement, userConfig: Object)// TODO: написать типы для userConfig надо
	{
		this.appElement = appElement
		const sessionListElement = appElement.querySelector('.sessions') as HTMLElement
	if (this.hasInitEverBeenStarted)
	{
		throw new Error('VDH:: already initialized!')
	}
	if (!userConfig)
	{
		throw new Error('VDH:: 2nd argument (userConfig) is empty')
	}
	if (userConfig instanceof Array)
	{
		userConfig = {images: userConfig}
	}
	//@ts-ignore TS2339
	else if (!userConfig.images)
	{
		throw new Error('VDH:: Не задан список картинок!')
	}
	//@ts-ignore TS2339
	if (!userConfig.images.length)
	{
		throw new Error('VDH:: Список картинок есть, но он пуст!')
	}
	this.hasInitEverBeenStarted = true
	this.supportsWebp = await supportsWebP
	appElement.style.display = ''
	//const beOnlyOnline: boolean = userConfig.p2p

	const looped = (
		//@ts-ignore TS2339
		typeof userConfig.looped === 'boolean'
		//@ts-ignore TS2339
		? userConfig.looped
		: (
			//@ts-ignore TS2339
			typeof userConfig.loop === 'boolean'
			//@ts-ignore TS2339
			? userConfig.loop
			: (
				//@ts-ignore TS2339
				typeof userConfig.isLooped === 'boolean'
				//@ts-ignore TS2339
				? userConfig.isLooped
				: (
					//@ts-ignore TS2339
					typeof userConfig.islooped === 'boolean'
					//@ts-ignore TS2339
					? userConfig.islooped
					: (
						//@ts-ignore TS2339
						typeof userConfig.isLoop === 'boolean'
						//@ts-ignore TS2339
						? userConfig.isLoop
						: (
							//@ts-ignore TS2339
							typeof userConfig.isloop === 'boolean'
							//@ts-ignore TS2339
							? userConfig.isloop
							: (
								//@ts-ignore TS2339
								typeof userConfig.carousel === 'boolean'
								//@ts-ignore TS2339
								? userConfig.carousel
								: (
									//@ts-ignore TS2339
									typeof userConfig.isCarousel === 'boolean'
									//@ts-ignore TS2339
									? userConfig.isCarousel
									: (
										//@ts-ignore TS2339
										typeof userConfig.iscarousel === 'boolean'
										//@ts-ignore TS2339
										? userConfig.iscarousel
										: true
									)
								)
							)
						)
					)
				)
			)
		)
	)
	const autoRotation = (
		//@ts-ignore TS2339
		typeof userConfig.autoRotation === 'boolean'
		//@ts-ignore TS2339
		? userConfig.autoRotation
		: (
			//@ts-ignore TS2339
			typeof userConfig.autorotation === 'boolean'
			//@ts-ignore TS2339
			? userConfig.autorotation
			: (
				//@ts-ignore TS2339
				typeof userConfig.autoRotate === 'boolean'
				//@ts-ignore TS2339
				? userConfig.autoRotate
				: (
					//@ts-ignore TS2339
					typeof userConfig.autorotate === 'boolean'
					//@ts-ignore TS2339
					? userConfig.autorotate
					: false
				)
			)
		)
	)

	const requestFullscreen = (
		(
			//@ts-ignore TS2339
			typeof userConfig.requestFullscreen === 'undefined'
			&&
			//@ts-ignore TS2339
			typeof userConfig.requestfullscreen === 'undefined'
			&&
			//@ts-ignore TS2339
			typeof userConfig.fullscreen === 'undefined'
		)
		? 'mobile'
		: (
			(
				//@ts-ignore TS2339
				userConfig.requestFullscreen === false
				||
				//@ts-ignore TS2339
				userConfig.requestfullscreen === false
				||
				//@ts-ignore TS2339
				userConfig.fullscreen === false
			)
			? false
			: (
				(
					//@ts-ignore TS2339
					userConfig.requestFullscreen === 'all'
					||
					//@ts-ignore TS2339
					userConfig.requestfullscreen === 'all'
					||
					//@ts-ignore TS2339
					userConfig.fullscreen === 'all'
				)
				? 'all'
				: 'mobile'
			)
		)
	)

	this.images = userConfig.images
	this.fixNewImagesArray(this.images)
	//await this.prepareForNewImageSizes()


	const animList: DocumentFragment = await this.imagesIntoDocumentFragment()
	{
		const c = animList.childNodes[0] as HTMLImageElement
		this.imageWidth  = c.width
		this.imageHeight = c.height
	}


	// Начало стилей (только тот css, который согласно моим попыткам, *необходимо* предвычислять в js).
	// Другие стили в отдельном .less-файле хранятся.
	const linkTag  = doc.createElement('style')
	linkTag.type = 'text/css'
	doc.head.appendChild(linkTag)
	let rescaledWidth:  number = 0
	let rescaledHeight: number = 0
	;(() => {
		let xLocked = false
		let reorientationPending = false
		//let timeoutsQueue: Array<number> = []
		const x = (): void => {
			if (xLocked) {console.log('locked');return}
			xLocked = true

			//console.log('%dx%d vs %dx%d (%dx%d)', this.imageWidth, this.imageHeight, sessionListElement.offsetWidth, sessionListElement.offsetHeight, window.innerWidth, window.innerHeight)
			const globalHeight: number = mathMin(this.imageHeight, sessionListElement.offsetHeight)//htmlTag.offsetHeight
			const globalWidth:  number = sessionListElement.offsetWidth//htmlTag.offsetWidth
			//rescaledHeight = mathMin(globalHeight, this.imageHeight)
			//console.log(globalWidth, htmlTag.offsetHeight)
			//console.log(doc.body.offsetWidth, doc.body.offsetHeight)
			if (this.imageHeight > globalHeight)
			{
				rescaledHeight = globalHeight
				rescaledWidth  = this.imageWidth * (globalHeight / this.imageHeight)
				if (rescaledWidth > globalWidth)
				{
					rescaledWidth  = globalWidth
					rescaledHeight = this.imageHeight * (globalWidth / this.imageWidth)
				}
			}
			else if (this.imageWidth > globalWidth)
			{
				rescaledWidth  = globalWidth
				rescaledHeight = this.imageHeight * (globalWidth / this.imageWidth)
				if (rescaledHeight > globalHeight)
				{
					rescaledHeight = globalHeight
					rescaledWidth  = this.imageWidth * (globalHeight / this.imageHeight)
				}
			}
			else
			{
				rescaledHeight = this.imageHeight
				rescaledWidth  = mathMin(globalWidth, this.imageWidth)
			}

			// fix webkit
			rescaledWidth  = Math.round(rescaledWidth)
			rescaledHeight = Math.round(rescaledHeight)

			/*console.log(rescaledWidth, rescaledHeight)
			less.globalVars = {
				'@app_width':  rescaledWidth  + 'px',
				'@app_height': rescaledHeight + 'px',
			}
			less.modifyVars({
				'@app_width':  rescaledWidth  + 'px',
				'@app_height': rescaledHeight + 'px',
			})
			linkTag.type = 'text/less'
			linkTag.innerHTML = (
`@app_width: ${rescaledWidth}px;
@app_height: ${rescaledHeight}px;
${lessFile}
`
			)
			less.refresh()
			//less.refreshStyles()
			*/
			linkTag.innerHTML = (function () {
				const anim_width  = rescaledWidth * 0.279
				const anim_height = anim_width * (rescaledHeight / rescaledWidth)

				console.log('%d %d', globalWidth, rescaledWidth)
				let ret = ''
				if (globalWidth <= 425 && rescaledWidth > 240)
					ret += `
.anim1 img {
	height: ${240 * (rescaledHeight / rescaledWidth)}px;
	width: 240px;
	margin: ${-(240 * (rescaledHeight / rescaledWidth)) / 2}px auto 0;
}
`
				else if (globalWidth <= 540 && rescaledWidth > 260)
					ret += `
.anim1 img {
	height: ${260 * (rescaledHeight / rescaledWidth)}px;
	width: 260px;
	margin: ${-(260 * (rescaledHeight / rescaledWidth)) / 2}px auto 0;
}
`
				else if (globalWidth <= 628 && rescaledWidth > 340)
					ret += `
.anim1 img {
	height: ${340 * (rescaledHeight / rescaledWidth)}px;
	width: 340px;
	margin: ${-(340 * (rescaledHeight / rescaledWidth)) / 2}px auto 0;
}
`
				else
					ret += `
.anim1 img {
	height: ${rescaledHeight}px;
	width: ${rescaledWidth}px;
	margin: ${-rescaledHeight / 2}px auto 0;
}
`
				if (doc.querySelector('#app[data-mode="client"][data-client-connected="true"]'))
				{
					ret += `
.wait_for_user_input {
	height: ${rescaledHeight}px;
}
					`
				}
				if (
					doc.querySelector('#app[data-mode="server"][data-onlyp2p="true"]')
					||
					doc.querySelector('#app[data-mode="client"]:not([data-client-connected="true"])')
				)
				{
					ret += `
#app {
	height: ${rescaledHeight}px;
	width: ${rescaledWidth}px !important;
}
	#app .anim, #app .anim img {
		height: ${anim_height}px;
		width: ${anim_width}px;
	}

	#app .anim1 {
		left: ${(rescaledWidth - anim_width) / 2 - anim_width + Math.ceil(anim_width / 8)}px;
		top: ${(rescaledHeight - anim_height) / 2}px;
	}
	#app .anim3 {
		right: ${(rescaledWidth - anim_width) / 2 - anim_width + Math.ceil(anim_width / 8)}px;
		top: ${(rescaledHeight - anim_height) / 2}px;
	}

	#app .anim2 {
		left: ${(rescaledWidth - anim_width) / 2}px;
		top: ${anim_width / 8}px;
	}
	#app .anim4 {
		bottom: ${anim_width / 8}px;
		left: ${(rescaledWidth - anim_width) / 2}px;
	}
`
				}
				return ret.replace(/(\.\d{3})\d+/g, '$1')
			})();

			dispatchEvent(newcssresize)

			xLocked = false
		}
		x()
		//addEventListener('resize', 

		let resizePending: boolean = false
		let lastOrientationChange: number = 0
		{
			const fn = () => {
				if (
					!reorientationPending
					&&
					dateNow() - lastOrientationChange > 60
				)
				{
					resizePending = true
					//console.log('window.onresize lastOrChange was %d ago', dateNow() - lastOrientationChange)
				}
			}
			addEventListener('resize', fn)
			addEventListener('fullscreen', fn)
		}
		let lastGlobalHeight: number = mathMin(this.imageHeight, sessionListElement.offsetHeight)
		let lastGlobalWidth:  number = sessionListElement.offsetWidth//htmlTag.offsetWidth
		const fn = (w) => {
			resizePending = false
			const newGlobalHeight: number = mathMin(this.imageHeight, sessionListElement.offsetHeight)
			const newGlobalWidth:  number = sessionListElement.offsetWidth//htmlTag.offsetWidth
			//console.log('fn(%d): old(%dx%d) vs new(%dx%d), xLocked = %O', w, lastGlobalWidth, lastGlobalHeight, newGlobalWidth, newGlobalHeight, xLocked)
			if (
				newGlobalHeight !== lastGlobalHeight
				||
				newGlobalWidth  !== lastGlobalWidth
			)
			{
				lastGlobalHeight = newGlobalHeight
				lastGlobalWidth  = newGlobalWidth
				x()
			}
		}
		addEventListener('orientationchange', () => {
			reorientationPending = true
			//console.log('window.orientationchange')
			lastOrientationChange = dateNow()
			setTimeout(() => {
				fn(2)
				lastOrientationChange = dateNow()
				reorientationPending = false
			}, 100)
		})
		setInterval(() => {
			if (!resizePending || reorientationPending || xLocked) return
			//console.log('setInterval: resize')
			fn(1)
		}, 300)
	})();
	// конец стилей



	//;(<any>window).$ = $
	{
		const b: HTMLInputElement = doc.querySelector('[name="role"][value="server"]') as HTMLInputElement
		if (b) b.click()
	}

	//@ts-ignore TS2339
	const thesocket = userConfig.p2p ? io(
		//@ts-ignore TS2339
		'wss://' + userConfig.p2p,
		{
			pingInterval: 2000,
			transports: ['websocket'/*, 'polling'*/]
		}
	) : null

	appElement.dataset.onlyp2p
		= htmlTag.dataset.onlyp2p
		= (thesocket ? 'true' : 'false')

	if (thesocket)
	{
		/*thesocket.p2p = new P2P(thesocket, {
			channelName: 'vdHoloplay',
			...peerSettings,
		})
		thesocket.p2p.on('ready', () => {
			thesocket.p2p.usePeerConnection = true
			console.log('WebRTC есть!')
			setTimeout(() => {
				if (thesocket.p2p.readyPeers) alert('WebRTC есть!')
			}, 2000)
		})*/
		/*thesocket.p2p.on('peer-test', (dd) => {
			console.log(dd)
		})
		setInterval(() => {
			thesocket.p2p.emit('peer-test', dateNow())
		}, 1000)*/

		await new Promise(resolve => {
			thesocket.on('connect', () => {
				console.warn('onConnect!')
				let haveWeBeenAssignedUuid: boolean = doc.cookie.includes(`${PROJECT_NAME}Uuid`)

				thesocket.on('newUuidAssignment', (newUuid: string) => {
					if (!newUuid || typeof newUuid !== 'string') return
					haveWeBeenAssignedUuid = false

					let expiresOn: Date = new Date()
					expiresOn.setTime(expiresOn.getTime() + (365*24*60*60*1000))
					doc.cookie = `${PROJECT_NAME}Uuid=${newUuid}; expires=${expiresOn.toUTCString()}; path=/` //
					if (!doc.cookie.includes(`${PROJECT_NAME}Uuid=${newUuid}`))
					{
						alert('Ошибка: у вас в настройках браузера отключены куки')
					}

					uuid = newUuid

					haveWeBeenAssignedUuid = true
					resolve()
				})

				if (haveWeBeenAssignedUuid) resolve()
			})
		})
		thesocket.on('reconnect', () => {
			console.warn('onReeeeeeeeeeeeeConnect!')
			location.reload()
		})
		thesocket.on('disconnect', () => {
			console.warn('onDiiiiiiisConnect!')
			htmlTag.classList.add('js-not-loaded')
		})

		{
			let whichTime: number = 0
			const updateDebugInfo = (): void => {
				;(doc.querySelector('.debug') as HTMLElement).innerHTML =
`Точность времени: +-${GoTime.getPrecision()} мс
<br>Время сервера быстрее нашего на ${GoTime.getOffset()} мс
<br>Синхронизировано раз: ${whichTime}`
			}
			GoTime.setOptions({
				//AjaxURL: "/ntp.php",
				//AjaxURL: "/ntp.nginx", // not the same on host and spectator, I hate it
				SyncInitialTimeouts: [0, 1000, 2000, 3000, 4000], // First set of syncs (ms from initialization) [default]
				SyncInterval: 10e3,       // Follow-up syncs happen at interval of 15 minutes [default]
				WhenSynced: function(time, method, offset, precision) {
					//console.log("Synced for %Oth time (time=%O, method=%O, offset=%O, precision=%O", ++whichTime, time, method, offset, precision)
					//updateDebugInfo()
				},
				OnSync: function(time, method, offset, precision){
					//console.log("Synced for %Oth time (time=%O, method=%O, offset=%O, precision=%O", ++whichTime, time, method, offset, precision)
					//updateDebugInfo()
				}
			})
			GoTime.wsSend(() => {
				const uid = dateNow()//Math.random()
				thesocket.emit('getTrackerTime', uid, (gotUid: number, trackerTime: number) => {
					if (uid === gotUid)
					{
						GoTime.wsReceived(trackerTime)
					}
					else
					{
						console.error('!!!!!!!!!!!!!! %O %O', uid, gotUid)
					}
				})
				return true
			})
		}
	}






	//const sessionListElement      = appElement.querySelector('.sessions') as HTMLElement
	//const settingsElement         = appElement.querySelector('.settings') as HTMLElement
	if (requestFullscreen !== false)
	{
		const waitForUserInputElement = appElement.querySelector('.wait_for_user_input') as HTMLElement

		const goFullscreen = (): void => {
			if (!DEV_MODE)
			{
				if (typeof htmlTag.requestFullscreen === 'function')
				{
					htmlTag.requestFullscreen()
				}
				else if (typeof (<any>document).documentElement.webkitRequestFullscreen === 'function')
				{
					;(<any>document).documentElement.webkitRequestFullscreen()
				}
				else if (typeof (<any>document).documentElement.mozRequestFullScreen === 'function')
				{
					;(<any>document).documentElement.mozRequestFullScreen() // FIXME: Get rid of <any>?
				}
				else if (typeof (<any>document).documentElement.msRequestFullscreen === 'function')
				{
					;(<any>document).documentElement.msRequestFullscreen() // FIXME: Get rid of <any>?
				}
			}
			waitForUserInputElement.style.display = 'none' // TODO: ??? back ???  or we not need it
			inputAccepted = true
		}
		waitForUserInputElement.onclick = goFullscreen;
		if (getComputedStyle(waitForUserInputElement).display === 'none')
		{
			inputAccepted = true
		}
	}
	else
	{
		inputAccepted = true
	}





	let artistSessionId: SessionIdType = Session.genSessionId()
	this.sessions[artistSessionId] = new artistSession(thesocket, artistSessionId, animList, sessionListElement, looped, autoRotation, this.frameRenderer)
	let spectatorSessionId: SessionIdType|null = null
	let listOfAllSessionIds: Set<SessionIdType> = new Set
	//let clientLastSync = null

	// ID сессии, по которому зритель будет подключаться
	// к артисту
	const inputSessionId: HTMLInputElement|null = appElement.querySelector('input[name="sessionId"]')
	if (thesocket && inputSessionId === null) return
	if (thesocket)
	{
		const redrawSessionList = (): void => {
			const el = appElement.querySelector('.sessionList') as HTMLElement
			el.innerHTML = ''
			//@ts-ignore TS2569
			for (const sessionId of listOfAllSessionIds)
			{
				if (sessionId === artistSessionId) continue
				const button = doc.createElement('BUTTON')
				//button.setAttribute('data-sessionid', sessionId.toString())
				button.innerHTML = sessionId.toString()
				el.appendChild(button)
				button.onclick = () => {
					thesocket.emit('joinSession', sessionId, (response) => {
						if (response.status !== 'ok')
						{
							console.error('joinSession.response: %O', response)
							return
						}
						Array.from(sessionListElement.children)
						.forEach(x => {
							if (x instanceof HTMLElement)
							{
								x.style.display = 'none'
							}
							else
							{
								console.warn('not instanceof HTMLElement: %O', x)
							}
						})
						const theNewSession = (
							this.sessions[sessionId]
							|| (
								this.sessions[sessionId]
								= new spectatorSession(
									thesocket,
									sessionId,
									animList,
									sessionListElement,
									this.frameRenderer
								)
							)
						)
						spectatorSessionId = sessionId
						appElement.dataset.clientConnected = 'true'
						if (response.latestItem)
						{
							theNewSession.activeLogVersion = response.latestItem.logVersion
							this.frameRenderer.addFrame(null, theNewSession, () => {
								theNewSession.setLocalState(response.latestItem.state)
							}, response.latestItem.state)
							/*theNewSession.addRemoteLogItems(
								response.latestItem.logVersion,
								response.latestItem.logVersion,
								[response.latestItem])*/
						}
					})
				}
			}
		}
		thesocket.on('listOfAllSessionIds', (sessionIds) => {
			console.log('listOfAllSessionIds: %O', sessionIds)
			listOfAllSessionIds = new Set(sessionIds)
			redrawSessionList()
		})
		thesocket.on('sessionDied', (sessionId) => {
			if (role === 'server')
			{
				throw new Error('why the f we received `sessionDied` if we role=server?')
			}
			if (spectatorSessionId !== sessionId)
			{
				throw new Error('why the f we received `sessionDied` if we are not its member?')
			}
			appElement.dataset.clientConnected = 'false'
			spectatorSessionId = null
			listOfAllSessionIds.delete(sessionId)
			this.sessions[sessionId].alive = false
			this.sessions[sessionId].sessionElement.parentNode!.removeChild(this.sessions[sessionId].sessionElement)
			if (
				Array.from(sessionListElement.children)
				.filter(
					x => x instanceof HTMLElement && x.style.display !== 'none'
				).length
				===
				0
			)
			{
				if (sessionListElement.children)
				{
					if (sessionListElement.children[0] instanceof HTMLElement)
					{
						sessionListElement.children[0].style.display = ''
					}
					else
					{
						console.error('on.sessionDied:: not instanceof HTMLElement: %O', sessionListElement.children[0])
					}
				}
			}
			redrawSessionList()
			/*setTimeout(() => {
				if (this.sessions[sessionId] && !this.sessions[sessionId].alive)
				{*/
					delete this.sessions[sessionId]
				/*}
			}, 30e3)*/
		})


		/*inputSessionId.on('input', () => {
			let newVal = inputSessionId.val().replace(/[^\d]/g, '')
			inputSessionId.val(newVal)
			if (/^\d{5}$/.test(newVal)) // if number of digits is changed then change it in genSessionId() too
			{
				if (role === 'server')
				{
					artistSessionId = +newVal // number!
				}
				else
				{
					spectatorSessionId = +newVal // number!
				}
			}
			else
			{
				if (role === 'server')
				{
					artistSessionId = null
				}
				else
				{
					spectatorSessionId = null
				}
			}
		})*/

	}

	const goServer = (): void => {
		if (appElement.dataset.clientConnected === 'true')
		{
			appElement.dataset.clientConnected = 'false'
			if (thesocket) thesocket.emit('leaveSession', spectatorSessionId)
		}
		spectatorSessionId = null
		//clientLastSync = null
		if (inputSessionId !== null)
		{
			inputSessionId.value  = artistSessionId.toString()
			//inputSessionId.setAttribute('readonly', '')
		}
		appElement.dataset.mode = htmlTag.dataset.mode = 'server'
		if (thesocket) thesocket.emit('hostSession', artistSessionId, (res) => {
			//console.log('hostSession.result = %O', res)
		})
	}
	const goClient = (): void => {
		if (thesocket) thesocket.emit('leaveSession', artistSessionId)
		spectatorSessionId = null
		//clientLastSync = null
		if (inputSessionId !== null)
		{
			inputSessionId.removeAttribute('readonly')
			inputSessionId.value = ''
			inputSessionId.focus()
		}
		appElement.dataset.mode = htmlTag.dataset.mode = 'client'
		//$window.scrollTop(1)
	}

	this.frameRenderer.start()

	goServer()



	if (thesocket)
	{
		const x = Array.from(appElement.querySelectorAll('input[name="role"]'))
		for (let i = 0; i < x.length; i++)
		{
			x[i].addEventListener('change', () => {
				role = (appElement.querySelector('input[name="role"]:checked') as HTMLInputElement).value as string
				if (role === 'server') goServer()
				else goClient()
			})
		}
		thesocket.on('xPing', (emitTime: number, respond: Function) => {
			//console.log('received xPing')
			respond(emitTime)
		})
		let lastReceivedEmitTime: number = 0
		{
			const fn = () => {
				const emitTime: number = dateNow()
				thesocket.emit('xPing', emitTime, (receivedEmitTime: number) => {
					if (emitTime === receivedEmitTime && receivedEmitTime > lastReceivedEmitTime)
					{
						lastReceivedEmitTime = receivedEmitTime
						const ret: number = dateNow() - emitTime
						//pingToTracker = MAXPING
						if (ret < 10) pingToTracker = FRAME_DURATION_IN_MS_INTEGER_CEIL * 2
						else if (ret > MAXPING) pingToTracker = MAXPING
						else pingToTracker = ret
						console.log('pingToTracker: ' + pingToTracker)
					}
				})
			}
			setTimeout(fn, 1000)
			setTimeout(fn, 4000)
			setInterval(fn, 10000)
		}

		// client only {
		
		// client only }

		// client only {
		/*thesocket.on('setState', (sessionId, data) => {
			//console.log(`data.sessionId (${data.sessionId}) vs spectatorSessionId (${spectatorSessionId})`)
			//console.log(`${clientLastSync} < ${data.time} is ${clientLastSync < data.time}`)
			//console.error('!socket.setState')
			if (role === 'client' && spectatorSessionId
			 && sessionId === spectatorSessionId)
			{
				if (locked) { console.error('wow, we are locked... data=%O', data); return; }
				if (!Array.isArray(data)) data = [data]
				if (data.length === 0) return
				//if (data.length === 1) { setLocalState(sessionId, data[0].state); return; }
				processNewLogItems(sessionId, data)
				/*if (clientLastSync === null)
				{
					appElement.dataset.clientConnected = 'true'
					//settingsElement.style.display = 'none'
					//for (const container of frameContainers)
					//{
					//	container.style.display = 'block'
					//}
				}
				if (clientLastSync === null || clientLastSync < data.time)
				{
					clientLastSync = data.timestamp
					setLocalState(sessionId, data.state)
				}*\/
			}
		})*/
		// client only }
	}
	} // end of .init

	protected async imagesIntoDocumentFragment (/*images: Array<string>*/): Promise<DocumentFragment>
	{
		const ret = doc.createDocumentFragment()
		const preloadList: Array<Promise<void>> = []
		const n: number = this.images.length
		let loaded: number = 0
		const wait_while_loading = this.appElement.querySelector('.wait_while_loading') as HTMLElement
		const wait_while_loading__progress = wait_while_loading.querySelector('.wait_while_loading__progress') as HTMLElement
		console.assert(wait_while_loading)
		console.assert(wait_while_loading__progress)
		wait_while_loading.style.display = ''
		for (let i = 0; i < n; i++)
		{
			const url = this.images[i]
			const img = doc.createElement('IMG') as HTMLImageElement
			img.setAttribute(
				'class',
				(
					url[0] === '~'
					? 'anim__img anim__img_active'
					: 'anim__img anim__img_hidden'
				)
			)
			img.setAttribute('alt', '')
			img.setAttribute('ondragstart', 'return false;')
			preloadList.push(new Promise(resolve => {
				img.onload = () => {
					loaded++
					wait_while_loading__progress.innerHTML = ' (' + Math.round(loaded/n*100) + '%)'
					resolve()
				}
				img.onerror = () => {
					ret.removeChild(img)
					loaded++
					wait_while_loading__progress.innerHTML = ' (' + Math.round(loaded/n*100) + '%)'
					resolve()
				}
			}))
			img.src = url[0] === '~' ? url.slice(1) : url
			ret.appendChild(img)
		}
		/*for await (const promise of preloadList)
		{
			promise.then(() => {
				yield 3
			})
		}*/
		await Promise.all(preloadList)
		wait_while_loading.style.display = 'none'
		return ret
		/*
		return this.images.map(x => 
			x[0] === '~'
			? '<img src="' + x.slice(1) + '" class="anim__img anim__img_active" alt="" ondragstart="return false;">'
			: '<img src="' + x + '" class="anim__img anim__img_hidden" alt="" ondragstart="return false;">'
		).join('')*/
	}

	public async setImages (newImages: Array<string>): Promise<void>
	{
		if (!(newImages instanceof Array))
		{
			throw new Error('VDH.setImages:: Требуется массив (из строк).')
		}
		if (!newImages.length)
		{
			throw new Error('VDH.setImages:: Требуется непустой массив.')
		}
		if (typeof newImages[0] !== 'string')
		{
			throw new Error('VDH.setImages:: Требуется массив именно из строк.')
		}

		this.fixNewImagesArray(newImages)
		if (this.images.join(',') === newImages.join(','))
		{
			throw new Error('VDH.setImages:: Картинки те же самые переданы, нечего менять.')
		}


		//await this.prepareForNewImageSizes()
		this.images = newImages
		const animList = await this.imagesIntoDocumentFragment()
		{
			const c = animList.childNodes[0] as HTMLImageElement
			this.imageWidth  = c.width
			this.imageHeight = c.height
		}
		this.frameRenderer.stop()
		for (const sessionId in this.sessions)
		{
			if (!this.sessions.hasOwnProperty(sessionId)) continue
			this.sessions[sessionId].updateAnimImages(animList)
		}
		this.frameRenderer.start()
	}
//} catch (e) { console.error(e); alert(e) }
}
;(window as any).VDH = VDH