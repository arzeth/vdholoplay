// Source: https://github.com/nicksardo/GoTime
export default class GoTime {
    static _syncCount = 0;

    static _offset = 0;

    static _precision = Infinity;

    static _history: Array<{
    	Sample: number,
       Method: string,
       Time: number
    }> = [];

    static _syncInitialTimeouts = [0, 3000, 9000, 18000, 45000];

    static _syncInterval = 900000;

    static _synchronizing = false;

    static _lastSyncTime = null;

    static _lastSyncMethod = null;

    static _ajaxURL = null;

    static _ajaxSampleSize = 1;

    static _firstSyncCallbackRan = false;

    static _firstSyncCallback: Function|null = null;

    static _onSyncCallback: Function|null = null;

    static _wsCall: Function|null = null;

    static _wsRequestTime: null|number = null;

    /*function GoTime() {
      GoTime._setupSync();
      return new Date(GoTime.now());
    }*/

    static _setupSync () {
      var time, _i, _len, _ref;
      if (GoTime._synchronizing === false) {
        GoTime._synchronizing = true;
        _ref = GoTime._syncInitialTimeouts;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          time = _ref[_i];
          setTimeout(GoTime._sync, time);
        }
        setInterval(GoTime._sync, GoTime._syncInterval);
      }
    }

    static now (): number {
      return Date.now() + GoTime._offset;
    }

    static getOffset (): number {
      return GoTime._offset;
    }

    static getPrecision (): number {
      return GoTime._precision;
    }

    static getLastMethod () {
      return GoTime._lastSyncMethod;
    }

    static getSyncCount (): number {
      return GoTime._syncCount;
    }

    static getHistory () {
      return GoTime._history;
    }

    static setOptions (options) {
      if (options.AjaxURL != null) {
        GoTime._ajaxURL = options.AjaxURL;
      }
      if (options.SyncInitialTimeouts != null) {
        GoTime._syncInitialTimeouts = options.SyncInitialTimeouts;
      }
      if (options.SyncInterval != null) {
        GoTime._syncInterval = options.SyncInterval;
      }
      if (options.OnSync != null) {
        GoTime._onSyncCallback = options.OnSync;
      }
      if (options.WhenSynced != null) {
        GoTime._firstSyncCallback = options.WhenSynced;
      }
      return GoTime._setupSync();
    }

    static wsSend (callback: Function) {
      return GoTime._wsCall = callback;
    }

    static wsReceived (serverTimeString: string|number) {
      var responseTime, sample, serverTime;
      responseTime = Date.now();
      serverTime = +serverTimeString// GoTime._dateFromService(serverTimeString);
      sample = GoTime._calculateOffset(GoTime._wsRequestTime, responseTime, serverTime);
      return GoTime._reviseOffset(sample, "websocket");
    }

    static _ajaxSample () {
      var req, requestTime;
      req = new XMLHttpRequest();
      req.open("GET", GoTime._ajaxURL);
      req.onreadystatechange = function() {
        var responseTime, sample, serverTime;
        responseTime = Date.now();
        if (req.readyState === 4) {
          if (req.status === 200) {
            //serverTime = GoTime._dateFromService(req.responseText);
            serverTime = GoTime._dateFromService(req.responseText.replace('.', '')); // my line
            sample = GoTime._calculateOffset(requestTime, responseTime, serverTime);
            GoTime._reviseOffset(sample, "ajax");
          }
        }
      };
      requestTime = Date.now();
      req.send();
      return true;
    }

    static _sync () {
      var success;
      if (GoTime._wsCall != null) {
        GoTime._wsRequestTime = Date.now();
        success = GoTime._wsCall();
        if (success) {
          GoTime._syncCount++;
          return;
        }
      }
      if (GoTime._ajaxURL != null) {
        success = GoTime._ajaxSample();
        if (success) {
          GoTime._syncCount++;
        }
      }
    }

    static _calculateOffset (requestTime, responseTime, serverTime) {
      requestTime  = requestTime  | 0
      responseTime = responseTime | 0
      serverTime   = serverTime   | 0
      var duration, oneway;
      duration = responseTime - requestTime;
      oneway = duration / 2;
      //console.log(+serverTime, +requestTime, +responseTime, oneway)
      return {
        offset: serverTime - requestTime - oneway,
        precision: oneway
      };
    }

    static _reviseOffset (sample, method) {
      var now;
      if (isNaN(sample.offset) || isNaN(sample.precision)) {
        return;
      }
      now = GoTime.now();
      GoTime._lastSyncTime = now;
      GoTime._lastSyncMethod = method;
      GoTime._history.push({
        Sample: sample,
        Method: method,
        Time: now
      });
      if (sample.precision <= GoTime._precision) {
        GoTime._offset = Math.round(sample.offset);
        GoTime._precision = sample.precision;
      }
      if (!GoTime._firstSyncCallbackRan && (GoTime._firstSyncCallback !== null)) {
        GoTime._firstSyncCallbackRan = true;
        return GoTime._firstSyncCallback(now, method, sample.offset, sample.precision);
      } else if (GoTime._onSyncCallback !== null) {
        return GoTime._onSyncCallback(now, method, sample.offset, sample.precision);
      }
    }

    static _dateFromService (text): Date {
      return new Date(parseInt(text));
    }
}
