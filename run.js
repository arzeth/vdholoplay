/**
 * React Static Boilerplate
 * https://github.com/kriasoft/react-static-boilerplate
 *
 * Copyright © 2015-present Kriasoft, LLC. All rights reserved.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

/* eslint-disable no-console, global-require */

const fs = require('fs');
const del = require('del');
const ejs = require('ejs');
const webpack = require('webpack');
const yaml = require('js-yaml')
const config = {}//yaml.load(fs.readFileSync('./app.yaml'))


const tasks = new Map(); // The collection of automation tasks ('clean', 'build', etc.)

function run(task) {
  const start = new Date();
  console.log(`Starting '${task}'...`);
  return Promise.resolve().then(() => tasks.get(task)()).then(() => {
    console.log(`Finished '${task}' after ${new Date().getTime() - start.getTime()}ms`);
  }, err => console.error(err.stack));
}

//
// Clean up the output directory
// -----------------------------------------------------------------------------
tasks.set('clean', () => del(['public/dist/*', '!public/dist/.git'], { dot: true }));

//
// Copy ./index.html into the /public folder
// -----------------------------------------------------------------------------
tasks.set('html', () => {
  for (const fname of fs.readdirSync('./'))
  {
    if (!/^app\..+\.yaml$/i.test(fname)) continue
    const config = yaml.load(fs.readFileSync(fname))
    if (!config.name)
    {
      config.name = fname.match(/^app\.(.+)\.yaml$/i)[1]
    }
    const webpackConfig = require('./webpack.config');
    const assets = JSON.parse(fs.readFileSync('./public/dist/assets.json', 'utf8'));
    const template = fs.readFileSync('./public/index.ejs', 'utf8');
    /*const onlyp2p = (function () {
      const f = fs.readFileSync('./app.yaml')
      if (!f) return true
      const m = f.toString().match(/^p2p:\s+(true|false)/m)
      if (!m) return true
      return m[1] === 'true'
    })()*/
    const onlyp2p = typeof config.p2p === 'string'
    const requestFullscreen = (function () {
      const f = fs.readFileSync(fname)
      if (!f) return 'mobile'
      const m = f.toString().match(/^(?:request)?Fullscreen:\s+(.+)$/im)
      if (!m) return 'mobile'
      if (m[1] === 'false') return false
      if (m[1] === 'all')   return 'all'
      throw new Error('requestFullscreen = `' + m[1] + ' `??')
    })();
    //const requestFullscreen = 
    const render = ejs.compile(template, { filename: './public/index.ejs' });
    const output = render({
      debug: webpackConfig.debug,
      bundle: assets.main.js,
      onlyp2p,
      requestFullscreen,
      config
    });
    fs.writeFileSync('./public/' + config.name + '.html', output, 'utf8');
  }
});

//
// Generate sitemap.xml
// -----------------------------------------------------------------------------
tasks.set('sitemap', () => {
  return;
  const urls = require('./routes.json')
    .filter(x => !x.path.includes(':'))
    .map(x => ({ loc: x.path }));
  const template = fs.readFileSync('./public/sitemap.ejs', 'utf8');
  const render = ejs.compile(template, { filename: './public/sitemap.ejs' });
  const output = render({ config, urls });
  fs.writeFileSync('public/sitemap.xml', output, 'utf8');
});

//
// Bundle JavaScript, CSS and image files with Webpack
// -----------------------------------------------------------------------------
tasks.set('bundle', () => {
  const webpackConfig = require('./webpack.config');
  return new Promise((resolve, reject) => {
    webpack(webpackConfig).run((err, stats) => {
      if (err) {
        reject(err);
      } else {
        console.log(stats.toString(webpackConfig.stats));
        resolve();
      }
    });
  });
});

//
// Build website into a distributable format
// -----------------------------------------------------------------------------
tasks.set('build', () => {
  global.DEBUG = process.argv.includes('--debug') || false;
  return Promise.resolve()
    .then(() => run('clean'))
    .then(() => run('bundle'))
    .then(() => run('html'))
    .then(() => run('sitemap'));
});


//
// Build website and launch it in a browser for testing (default)
// -----------------------------------------------------------------------------
tasks.set('start', () => {
  let count = 0;
  //global.HMR = !process.argv.includes('--no-hmr'); // Hot Module Replacement (HMR)
  return run('clean').then(() => new Promise(resolve => {
    //const bs = require('browser-sync').create();
    const webpackConfig = require('./webpack.config');
    const compiler = webpack(webpackConfig);
    // Node.js middleware that compiles application in watch mode with HMR support
    // http://webpack.github.io/docs/webpack-dev-middleware.html
    /*const webpackDevMiddleware = require('webpack-dev-middleware')(compiler, {
      publicPath: webpackConfig.output.publicPath,
      stats: webpackConfig.stats,
    });*/
    compiler.plugin('done', stats => {
      // Generate index.html page
      const bundle = stats.compilation.chunks.find(x => x.name === 'main').files[0];
      const template = fs.readFileSync('./public/index.ejs', 'utf8');
      const render = ejs.compile(template, { filename: './public/index.ejs' });
      const output = render({ debug: true, bundle: `dist/${bundle}`, config });
      fs.writeFileSync('./public/index.html', output, 'utf8');

      // Launch Browsersync after the initial bundling is complete
      // For more information visit https://browsersync.io/docs/options
      /*if (++count === 1) {
        bs.init({
          port: process.env.PORT || 3000,
          ui: { port: Number(process.env.PORT || 3000) + 1 },
          server: {
            baseDir: 'public',
            middleware: [
              webpackDevMiddleware,
              require('webpack-hot-middleware')(compiler),
              require('connect-history-api-fallback')(),
            ],
          },
        }, resolve);
      }*/
    });
  }));
});

// Execute the specified task or default one. E.g.: node run build
run(/^\w/.test(process.argv[2] || '') ? process.argv[2] : 'start' /* default */);
